# Quick start

## Prepare

- Assume that you haven't cloned the code yet.

```bash
git clone git@gitlab.com:pcp-dashboard/pcp-dashboard-apps.git
```

- The webpack config have already wrapped to a submodule for the reuse poupose. You have to get it after clone.

```bash
git submodule init
git submodule update
```

## Initialize

Install package dependencies.

```bash
npm install
```

Incase, you prefer **yarn**.

```bash
yarn
```

## Start coding

Start your development hours by run this command.

```bash
modd
```
