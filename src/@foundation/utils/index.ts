function ENV(key: string, def: any = '') {
    const _key = `REACT_APP_${key}`;
    return process.env[_key] || def;
}

export { ENV };