import React from 'react';
import Loading from '@foundation/component/loading';

interface RouteConfig {
    dir?: string,
    loader: any,
    Placeholder?: any
}


function genAsyncComponent({ loader, Placeholder }: RouteConfig) {
    return class AsyncRouteComponent extends React.Component<any, any, any> {
        constructor(props: any) {
            super(props);
            this.state = {
                Component: null,
            };
        }
        componentDidMount() {
            loader().then(this.updateState);
        }
        updateState = (ResolvedComponent: any) => {
            const { default: Component } = ResolvedComponent;
            this.setState({
                Component,
            });
        }
        render() {
            const { Component } = this.state;

            if (Component) {
                return (<Component {...this.props} />);
            }

            if (Placeholder) {
                return (<Placeholder {...this.props} />);
            }

            return (
                <Loading title="Loading component"/>
            );
        }
    };
}

const LazyComponent = (loader: any) => genAsyncComponent({loader});



export default genAsyncComponent;
export { LazyComponent };
