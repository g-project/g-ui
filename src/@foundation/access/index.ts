/* eslint-disable @typescript-eslint/explicit-member-accessibility */
import axios from 'axios';
import { API_URL, AUTH_URL, REPORT_URL } from 'config';


class DataAccess {
    _profile: string
    _maner: any

    static INSTANCE: { [index: string]: DataAccess } = {}

    static P(profile: string): DataAccess {
        // To get profile by DataAccess
        return DataAccess.INSTANCE[profile];
    }

    constructor(profile: string, base_url: string, opt?: any) {
        this._profile = profile;

        if (!DataAccess.INSTANCE[profile]) {
            DataAccess.INSTANCE[profile] = this;
        }
        this._maner = axios.create({
            baseURL: base_url,
            timeout: 10000,
            withCredentials: true
        });
        DataAccess.INSTANCE[profile] = this;
        return DataAccess.INSTANCE[profile];
    }

    _request(method: string, args: any) {
        return this._maner({
            method,
            ...args
        });
    }

    async Get(url: string, args?: any) {
        return await this._request('get', {url, ...args});
    }
    
    async Post(url: string, args?: any) {
        return await this._request('post', {url, ...args});
    }
    
    async Update(url: string, args?: any) {
        return await this._request('patch', {url, ...args});
    }
    
    async Replace(url: string, args?: any) {
        return await this._request('put', {url, ...args});
    }
    
    async Delete(url: string, args?: any) {
        return await this._request('delete', {url, ...args});
    }
}

export default DataAccess;

// Setup datalayer
export const DataProfile = new DataAccess('data', API_URL);
export const OAuthProfile = new DataAccess('auth', AUTH_URL);
export const ReportProfile = new DataAccess('report', REPORT_URL);
