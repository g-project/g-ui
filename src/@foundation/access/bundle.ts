const  OfflineBundle = async (data: any, query: any) => {
    const _pagination = query['pagination'];
    const total = data.length;
    const from = (_pagination.page - 1) * _pagination.max_results;
    const to = from + _pagination.max_results;

    const bundle = {
        '_items': data.slice(from, to),
        '_pagination': {
            page: _pagination.page,
            max_results: _pagination.max_results,
            total
        }
    };
    return bundle;
};

export { OfflineBundle };
