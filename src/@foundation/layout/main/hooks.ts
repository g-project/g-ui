import { useDispatch } from 'react-redux';
import QS from 'qs';
import React from 'react';
import { push } from 'connected-react-router';

export const useLayout: any = () => {
    const dispatch = useDispatch();

    const applyParam = React.useCallback(_applyParam, []);
    
    function getParam(key: string) {
        const search = window.location.search;
        const params = QS.parse(search, { ignoreQueryPrefix: true });
        if (key) {
            if (!params[key]) {
                return null;
            }

            return JSON.parse(params[key]);
        }
        return params;
    }

    function _applyParam(key: string, value: any) {
        const search = window.location.search;
        const p = QS.parse(search, { ignoreQueryPrefix: true, plainObjects: false });
        p[key] = JSON.stringify(value);
        const news = QS.stringify(p);
        dispatch(push({
            search: news
        }));
    }

    return [getParam, applyParam];
};