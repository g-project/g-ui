export type MainLayoutProp = {
    baseUrl: string,
    module: string,
    layout: any,
};

export type RouteParam = {
    primary: string,
    second: string,
    ext: string
};

export type MainLayoutPartType = {
    params: string,
    type: string,
    applyParam: (key: string, value: any) => void
}