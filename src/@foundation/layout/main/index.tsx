import React from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';
import ComponentRegistry from '@foundation/registry/component';
import { useLayout } from './hooks';

import styles from './style.module.scss';
import { MainLayoutProp, RouteParam } from './main';

const QUERY_KEY_MAP = {
    primary: 'q',
    secondary: 'qs',
    extension: 'qe',
};

export default function MainLayout({ baseUrl, module, layout }: MainLayoutProp): JSX.Element {
    const [getParam, applyParam] = useLayout();

    function _panel(key: string, module: string) {
        return (props: RouteComponentProps<RouteParam>) => {
            const compId = props.match.params[key];
            const Comp = ComponentRegistry.get(module, compId) as any;
            return Comp ? <Comp
                params={getParam(QUERY_KEY_MAP[key])}
                applyParam={applyParam}
                type={key}
            /> : null;
        };
    }

    console.log('MainLayout::render');
    return (
        <div className={styles.mainLayout}>
            <div style={layout.extension}>
                <Route
                    path={baseUrl + '/:primary/:secondary/:extension'}
                    render={_panel('extension', module)}
                />
            </div>
            <div style={layout.primary}>
                <Route
                    path={baseUrl + '/:primary'}
                    render={_panel('primary', module)}
                />
            </div>
            <div style={layout.secondary}>
                <Route
                    path={baseUrl + '/:primary/:secondary'}
                    render={_panel('secondary', module)}
                />
            </div>
        </div>
    );
}
