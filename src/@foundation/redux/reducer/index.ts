import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router/seamless-immutable';
import StateRegistry from '@foundation/registry/state';

const rootReducer = (history: any) => {
    const appReducer = combineReducers({
        router: connectRouter(history),
        ...StateRegistry.ALL_REDUCER,
    });

    const _reducer = (state: any, action: any) => {
        const { type } = action;
        switch (type) {
        case 'RS':
            state = undefined;
        }

        return appReducer(state, action);
    };
    return _reducer;
};

export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
