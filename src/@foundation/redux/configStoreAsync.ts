import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { routerMiddleware } from 'connected-react-router';
import { seamlessImmutableReconciler, seamlessImmutableTransformCreator } from 'redux-persist-seamless-immutable';
import storage from 'redux-persist/lib/storage';
import rootReducer from './reducer';

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: seamlessImmutableReconciler,
    transforms: [seamlessImmutableTransformCreator({})],
    blacklist: ['router']
};

const configStoreAsync = (history: any): any => new Promise((resolve, reject) => {
    const store = createStore(
        persistReducer(persistConfig, rootReducer(history)),
        composeEnhancers(
            applyMiddleware(
                routerMiddleware(history),
            )
        )
    );
    persistStore(store, undefined, () => {
        resolve(store);
    });
});

export default configStoreAsync;
