declare module '*.scss' {
    const content: {[className: string]: string};
    export = content;
}

declare module '*.less' {
    const content: {[className: string]: string};
    export = content;
}

declare module '*.css' {
    const content: {[className: string]: string};
    export = content;
}

declare module '*.svg' {
    const content: {[className: string]: string, ReactComponent: any};
    export = content;
}

declare module '*.png' {
    const value: any;
    export = value;
 }