declare type MainLayoutPartType = {
    params: string,
    type: string,
    applyParam: (key: string, value: any) => void
}