import React from 'react';
import { Icon } from 'antd';
import SelectBox from '@foundation/component/select_box';
import SimpleDropdown from '@foundation/component/dropdown';
import { FilterItemSpec } from './widget';
import WIDGET from './widget';
import { GROUP_OP } from './const';

type FilterGroupItemProp = {
    root?: boolean,
    icon?: any,
    label?: string,
    path: string[],
    spec: any,
    options: FilterItemSpec[],
    onChange: (path: string[], value: any) => void,
    onAddItem: (path: string[], value: any) => void,
    onClose: (path: string[]) => void,
    onNegateItemValue: (path: string[]) => void,
    getCurrentFilter: () => any,
}

const formatOptionLabel = (data: any) => {
    const { label, op } = data.value;
    return <div className="flex justify-between">
        {label && <span>{label}</span>}
        <span>{op}</span>
    </div>;
};


const FilterGroupItem = ({ root, path, spec, label, onChange, onClose, onAddItem, onNegateItemValue, options, getCurrentFilter, icon }: FilterGroupItemProp) => {
    function renderItem(spec: any) {
        const _result: any = [];
        if (spec.size === 0) {
            return <div className="flex justify-center p-xs">
                <span>No Item</span>
            </div>;
        }

        spec.forEach((item: any, index: any) => {
            if (GROUP_OP.includes(item.get('op'))) {
                let _path = item.get('path');
                if (!_path) {
                    _path = [...path, index, 'value'];
                }
                _path = [..._path, 'value'];

                // Only allow group operator at level 1
                options = options.filter(item => !GROUP_OP.includes(item.op));

                _result.push(<FilterGroupItem
                    root={false}
                    key={_path.join('|')}
                    onClose={onClose}
                    onChange={onChange}
                    onNegateItemValue={onNegateItemValue}
                    path={_path}
                    spec={item.get('value')}
                    onAddItem={onAddItem}
                    options={options}
                    label={item.get('op')}
                    getCurrentFilter={getCurrentFilter}
                />);
            } else {
                if (item.get('widget')) {
                    const widget = item.get('widget');
                    let _path = item.get('path');
                    if (!_path) {
                        _path = [...path, index];
                    }
                    const FilterItem = WIDGET[widget];
                    _result.push(<FilterItem
                        key={_path.join('|')}
                        onClose={onClose}
                        onChange={onChange}
                        onNegateItemValue={onNegateItemValue}
                        path={_path}
                        spec={item.toJS()}
                        getCurrentFilter={getCurrentFilter}
                    />);
                }
            }
        });
        return _result;
    }
    function onAddFilter(item: FilterItemSpec) {
        onAddItem(path, item);
    }
    return (
        <div className={`mb-md ${root ? '' : 'pr-0 rounded-sm rounded-bl-none mt-sm'}`}>
            <SimpleDropdown
                overlay={<SelectBox
                    menuIsOpen autoFocus
                    options={options}
                    onSelect={onAddFilter}
                    clearAfterSelect
                    menuPlacement="bottom"
                    formatOptionLabel={formatOptionLabel}
                />}
                className="w-full"
                replace
            >
                {root ?
                    <div className="flex items-center justify-between bg-primary p-xs rounded-sm rounded-bl-none">
                        <div className="flex items-center">
                            {icon && <div className="flex items-center mx-sm">
                                {icon}
                            </div>}
                            <span className="text-base font-medium mr-md">{label}</span>
                        </div>
                        <Icon type="plus" className="border border-primary p-xs rounded-xs hover:bg-primary" />
                    </div>
                    :
                    <div className="flex bg-blue-200 justify-between items-center p-1">
                        <span className="flex text-sm font-medium items-center">
                            {`${label && label.toUpperCase()}`}
                            <Icon type="plus" className="ml-md" />
                        </span>
                        <Icon
                            className="text-red-300 hover:text-red-600"
                            type="close"
                            onClick={(e) => { e.nativeEvent.stopImmediatePropagation(); onClose(path); }}
                        />
                    </div>
                }
            </SimpleDropdown>
            <div className={`${root ? 'border-primary' : 'border-blue-200 border-2 p-sm border-t-0 bg-blue-100'} flex flex-col pl-sm border-l-2 border-dashed`}>
                {renderItem(spec)}
            </div>
        </div>
    );
};

export default FilterGroupItem;
