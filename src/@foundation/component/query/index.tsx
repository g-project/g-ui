import React, { useState, useEffect } from 'react';
import Immutable from 'immutable';
import { Icon, Button } from 'antd';
import { Loading } from '@foundation/component';
import { DataProfile } from '@foundation/access';

import { FilterItemSpec } from './widget';
import FilterGroupItem from './filter_group';
import { parseFilter } from './utils';
import { useFilter } from './hooks';

type QueryFilterProp = {
    Header: React.ReactType,
    title: string,
    applyFilter: any,
    defaultFilter: any,
    orderOption: any,
    filterOption: any
}

const DynamicQueryFilter = ({ source, applyFilter, Header, defaultFilter }: any) => {
    const [filterOption, setFilterOpt] = useState<FilterItemSpec[]>([]);
    const [orderOption, setOrderOpt] = useState<FilterItemSpec[]>([]);
    const [loading, setLoading] = useState(false);
    const [specification, setSpec] = useState([{}]);
    const [title, setTitle] = useState();
    const [error, setError] = useState();

    const filter = React.useMemo(() => parseFilter(specification, Immutable.fromJS(defaultFilter)).toJS(), [specification, defaultFilter]);

    useEffect(() => {
        if (source) {
            setLoading(true);
            // eslint-disable-next-line no-unused-expressions
            DataProfile.Get(source)
                .then((response: any) => {
                    let specification: FilterItemSpec[] = [];
                    const filters: FilterItemSpec[] = response.data['filters'];
                    const fields: FilterItemSpec = response.data['fields'];
                    const orders = response.data['orders'];
                    setTitle(response.data['title']);
                    specification = filters.map((item: FilterItemSpec) => {
                        const label = (fields[item.fn] && fields[item.fn].label) || '';
                        return {
                            ...item,
                            label,
                        };
                    });
                    const _orderSpec: Array<FilterItemSpec> = [];
                    for (let key of orders) {
                        const _filterItem = specification.find((item: any) => item.key.split(':')[0] === key);
                        if (_filterItem) {
                            const _orderItem = {
                                id: _filterItem['id'],
                                key: `${key}:sort`,
                                value: 1,
                                label: fields[_filterItem.fn].label,
                                widget: 'sort',
                                op: 'Sort',
                                dsc: '',
                                fn: _filterItem['fn']
                            };
                            _orderSpec.push(_orderItem);
                            specification.push(_orderItem);
                        }
                    }
                    const _filterSpec: Array<FilterItemSpec> = specification.filter((item: any) => item.key.split(':')[1] !== 'sort');
                    setSpec(specification);
                    setFilterOpt(_filterSpec);
                    setOrderOpt(_orderSpec);
                    setLoading(false);
                    setError(null);
                }) as unknown as FilterItemSpec[];
        } else {
            setError('Need to specific a source of filter');
        }

    }, [source]);

    if (loading) {
        return (
            <div style={{ display: 'flex', flex: '2 2 0', width: 250 }}>
                <Loading />
            </div>
        );
    }

    if (error) {
        return (
            <span className="p-md text-center">{error}</span>
        );
    }

    return (
        <QueryFilter
            Header={Header}
            title={title}
            applyFilter={applyFilter}
            defaultFilter={filter}
            filterOption={filterOption}
            orderOption={orderOption}
        />
    );
};

const QueryFilter = ({ Header, applyFilter, defaultFilter, filterOption, orderOption, title }: QueryFilterProp) => {
    const [state, [filterValue, orderValue], onChangeItemValue, onDeleteItem, onAddItem, onNegateItemValue, [isChange, clearChange], getCurrentFilter] = useFilter(defaultFilter);
    const [isCompact, setCompact] = useState(false);

    function _applyFilter(data: any) {
        clearChange();
        applyFilter(data);
    }
    if (isCompact) {
        return (
            <div className="flex flex-col" style={{ flex: '2 2 0'}}>
                <span className="text-base font-medium" style={{ writingMode: 'vertical-rl' }}>Filter</span>
                <Icon onClick={() => setCompact(false)} className="border rounded hover:bg-primary p-xs mt-sm" type="double-right" style={{ fontSize: 15 }} />
            </div>
        );
    }
    return (
        <div className="flex flex-col flex-1" style={{ minWidth: isCompact ? 0 : 250 }}>
            <Header disabled={!isChange} data={state} title={title} setCompact={setCompact} />
            <FilterGroupItem
                root={true}
                icon={<Icon type="filter" />}
                path={[]}
                spec={filterValue}
                options={filterOption}
                onChange={onChangeItemValue}
                onClose={onDeleteItem}
                onAddItem={onAddItem}
                label="Filter"
                onNegateItemValue={onNegateItemValue}
                getCurrentFilter={getCurrentFilter}
            />
            <FilterGroupItem
                root={true}
                icon={<Icon type="sort-ascending" />}
                path={[]}
                label="Order"
                spec={orderValue}
                options={orderOption}
                onChange={onChangeItemValue}
                onClose={onDeleteItem}
                onAddItem={onAddItem}
                onNegateItemValue={onNegateItemValue}
                getCurrentFilter={getCurrentFilter}
            />
            <Button disabled={!isChange} onClick={() => _applyFilter(state)} type="primary" className="mt-md">Apply</Button>
        </div>
    );
};

export default DynamicQueryFilter;
export { useFilter };