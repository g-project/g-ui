import React, { useState, useEffect } from 'react';
import { Icon, Slider, DatePicker, Switch } from 'antd';
import moment from 'moment';
import Select from 'react-select';
import {DataProfile} from '@foundation/access';
import { Loading } from '@foundation/component';


export type FilterItemSpec = {
    dsc: string,
    fn: string,
    id: string | number,
    key: string,
    label: string,
    op: string,
    params?: Record<string, any>,
    value: string | number | any,
    widget: string
}

export type FilterItemProp = {
    path: string[],
    spec: FilterItemSpec,
    onChange: (path: string[], value: any) => void,
    onClose: (path: string[]) => void,
    onNegateItemValue: (path: string[]) => void,
    getCurrentFilter: () => any,
}

const ItemContainerStyle = 'flex flex-col';
const CloseButtonStyle = 'rounded-sm text-red-300 hover:text-red-600 border hover:border-red-600 m-1 p-1 mr-0';
const WidgetStyle = {
};


const InputFilterItem = ({ path, spec, onChange, onClose, onNegateItemValue }: FilterItemProp) => {
    function _onChange(e: React.ChangeEvent<HTMLInputElement>) {
        const value = e.target.value;
        onChange(path, value);
    }
    const activeStyle = 'text-blue-600 border-blue-600';
    const negative = spec.key.includes('!');
    const options = spec.params ? spec.params : {};
    return (
        <div style={WidgetStyle}>
            <div key={path.join('|')} className={ItemContainerStyle}>
                <div className="flex flex-grow items-center mt-xs justify-between">
                    <div>
                        <span className="font-medium text-sm">{spec.label}</span>
                        <span className="font-medium text-sm border border-primary bg-second ml-sm p-px rounded-sm">{spec.op.toLowerCase()}</span>
                    </div>
                    <div className="flex flex-no-wrap">
                        <Icon onClick={() => onNegateItemValue(path)} type="exclamation" className={`rounded-sm text-blue-300 m-1 p-1 border ${negative ? activeStyle : ''}`} />
                        <Icon onClick={() => onClose(path)} className={CloseButtonStyle} type="close" />
                    </div>
                </div>
                <input
                    value={spec.value}
                    className="flex flex-1 w-10 w-full border border-second px-sm py-xs rounded focus:outline-none focus:border-gray-500"
                    placeholder={spec.dsc}
                    onChange={_onChange}
                    {...options}
                />
            </div>
        </div>
    );
};

const SelectFilterItem = ({ path, spec, onChange, onClose, onNegateItemValue, getCurrentFilter }: FilterItemProp) => {
    const [options,] = useState([{ label: 'thien', value: 'thien' }, { label: 'marsch', value: 'marsch' }]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (spec.params && spec.params.source) {
            setLoading(true);
            DataProfile.Post(spec.params.source, { data: getCurrentFilter().toJS() })
                .then((data: any) => {
                    setLoading(false);
                });
        }
    }, [getCurrentFilter, spec.params]);

    function _onChange(data: any) {
        onChange(path, data.value);
    }

    const activeStyle = 'text-blue-600 border-blue-600';
    const negative = spec.key.includes('!');
    const params = spec.params ? spec.params : {};

    return (
        <div style={WidgetStyle}>
            <div key={path.join('|')} className={ItemContainerStyle}>
                <div className="flex flex-grow items-center mt-xs justify-between">
                    <div>
                        <span className="font-medium text-sm">{spec.label}</span>
                        <span className="font-medium text-sm border border-primary bg-second ml-sm p-px rounded-sm">{spec.op.toLowerCase()}</span>
                    </div>
                    <div className="flex flex-no-wrap">
                        <Icon onClick={() => onNegateItemValue(path)} type="exclamation" className={`rounded-sm text-blue-300 m-1 p-1 border ${negative ? activeStyle : ''}`} />
                        <Icon onClick={() => onClose(path)} className={CloseButtonStyle} type="close" />
                    </div>
                </div>
                {!loading ?
                    <Select
                        styles={{
                            container: (base) => ({ ...base, display: 'flex', flex: 1, padding: 0 }),
                            control: (base) => ({ ...base, display: 'flex', flex: 1, borderColor: 'hsla(0, 0%, 80%, 0.46);', borderRadius: 3, minHeight: '32px' }),
                            indicatorsContainer: (base) => ({ ...base, padding: '0px' }),
                            input: (base, state) => ({ display: 'flex' })
                        }}
                        defaultValue={{ label: spec.value, value: spec.value }}
                        onChange={_onChange}
                        options={options}
                        components={{ DropdownIndicator: null, IndicatorSeparator: null }}
                        {...params}
                    /> :
                    <Loading size={30} />
                }
            </div>
        </div>
    );
};

const SliderFilterItem = ({ path, spec, onChange, onClose, onNegateItemValue }: FilterItemProp) => {
    function _onChange(value: any) {
        onChange(path, value);
    }
    const activeStyle = 'text-blue-600 border-blue-600';
    const negative = spec.key.includes('!');
    return (
        <div style={WidgetStyle}>
            <div key={spec.key} className={ItemContainerStyle}>
                <div className="flex flex-grow items-center mt-xs justify-between">
                    <span className="font-medium font-sm">{spec.label}:</span>
                    <div className="flex flex-no-wrap">
                        <Icon onClick={() => onNegateItemValue(path)} type="exclamation" className={`rounded-sm text-blue-300 m-1 p-1 border ${negative ? activeStyle : ''}`} />
                        <Icon onClick={() => onClose(path)} className={CloseButtonStyle} type="close" />
                    </div>
                </div>
                <div className="flex items-center">
                    <Slider
                        style={{ display: 'flex', flex: 1, paddingRight: 15 }}
                        value={spec.value}
                        onChange={_onChange}
                        {...spec.params}
                    />
                    <span>{spec.value}</span>
                </div>
            </div>
        </div>
    );
};

const DateFilterItem = ({ path, spec, onChange, onClose, onNegateItemValue }: FilterItemProp) => {
    function _onChange(date: any, dateString: any) {
        console.log('=>', date, dateString);
        onChange(path, dateString);
    }

    const activeStyle = 'text-blue-600 border-blue-600';
    const negative = spec.key.includes('!');

    return (
        <div style={WidgetStyle}>
            <div key={spec.key} className={ItemContainerStyle}>
                <div className="flex flex-grow items-center tb-xs justify-between">
                    <span className="font-medium text-sm">{spec.label}:</span>
                    <div className="flex flex-no-wrap">
                        <Icon onClick={() => onNegateItemValue(path)} type="exclamation" className={`rounded-sm text-blue-300 m-1 p-1 border ${negative ? activeStyle : ''}`} />
                        <Icon onClick={() => onClose(path)} className={CloseButtonStyle} type="close" />
                    </div>
                </div>
                <div className="flex justify-end">
                    <DatePicker
                        className="flex"
                        value={spec.value ? moment(spec.value) : undefined}
                        onChange={_onChange}
                    />
                </div>
            </div>
        </div>
    );
};

const SortItem = ({ path, spec, onChange, onClose, onNegateItemValue }: FilterItemProp) => {
    function _onChange(check: any) {
        onChange(path, check ? 1 : -1);
    }
    return (
        <div key={spec.key} className={ItemContainerStyle} style={{}}>
            <div className="flex flex-grow items-center" style={WidgetStyle}>
                <span className="font-medium text-sm flex-grow">{spec.label}:</span>
                <div className="flex items-center">
                    <Switch checkedChildren="ASC" unCheckedChildren="DSC" checked={spec.value === 1} onChange={_onChange} />
                    <Icon
                        onClick={() => onClose(path)}
                        className="rounded-sm text-red-300 hover:text-red-600 border hover:border-red-600  m-1 p-1 mr-0"
                        type="close"
                    />
                </div>
            </div>
        </div>
    );
};

export default {
    date: DateFilterItem,
    input: InputFilterItem,
    select: SelectFilterItem,
    slider: SliderFilterItem,
    sort: SortItem,
    text: InputFilterItem,
};