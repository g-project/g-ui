import React from 'react';
import { create } from 'react-test-renderer';
import QueryFilter from './index';


describe('Footer component', () => {
    test('Matches the snapshot', () => {
        const button = create(<QueryFilter />);
        expect(button.toJSON()).toMatchSnapshot();
    });
});