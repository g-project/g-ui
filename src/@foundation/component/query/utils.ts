import { GROUP_OP } from './const';
import Immutable from 'immutable';


function compileFilter(data: any) {
    // Transform filterValue to data
    let result = Immutable.List();

    data.forEach((item: any, index: number) => {
        const value = item.get('value');
        let _result = Immutable.fromJS({ [item.get('key')]: value });

        if (GROUP_OP.includes(item.get('op'))) {
            _result = _result.set(item.get('key'), compileFilter(value));
        }

        result = result.push(_result);
    });
    return result;
};

function parseFilter(specification: any, data: any) {
    // Transform data with speccification to filterValue to render QueryFilter component
    let result = Immutable.List();
    data.forEach((item: any, index: number) => {
        try {
            const _key = item.toArray()[0][0].toString();
            const _value = item.toArray()[0][1];
            
            // Return a new instance of specification item otherwise it will issue a side effect
            const _spec = {...specification.find((_item: any) => _item.key === _key.replace('!', ':'))};
            
            if (_spec) {
                _spec.value = _value;
                _spec.key = _key;
        
                if (_value) {
                    const op = _key.replace(':', '').toUpperCase();
                    if (GROUP_OP.includes(op)) {
                        _spec.value = parseFilter(specification, _value);
                    }
                }
        
                result = result.push(Immutable.fromJS({..._spec}));
            } else {
                console.error('Can not found', _key);
            }
        } catch (e) {
            console.error('Parse filter error:error', e);
            console.error('Parse filter error:data', data, specification);
            return result;
        }
    });
    return result;
}

export { compileFilter, parseFilter };