import { useState, useEffect, useMemo, useCallback } from 'react';
import Immutable from 'immutable';

const DEFAULT_VALUE = {
    default: '',
    text: '',
    input: '',
    slider: 0,
    date: '',
    sort: 1
};

export const useFilter: any = (DEFAULT_FILTER: any = []) => {
    /*
        - Filter is a tree
        - This is hook use to control the filter
    */
    DEFAULT_FILTER = DEFAULT_FILTER || [];
    const [_state, setState] = useState(Immutable.fromJS(DEFAULT_FILTER));
    const [isChange, setChange] = useState(false);

    const state = useMemo(() => _state, [_state]);
    const getCurrentFilter = useCallback(() => state, [state]);

    useEffect(() => {
        setState(Immutable.fromJS(DEFAULT_FILTER));
    }, [DEFAULT_FILTER]);

    function onChangeItemValue(key: string[], value: any) {
        const _state = state.setIn([...key, 'value'], value);
        setState(_state);
        setChange(true);
    }
    function onNegateItemValue(path: string[]) {
        let key = state.getIn([...path, 'key']);
        key = key.includes(':') ? key.replace(':', '!') : key.replace('!', ':');
        const _state = state.setIn([...path, 'key'], key);
        setState(_state);
        setChange(true);
    }
    function onAddItem(key: string[], item: any) {
        console.log('State', key, state.toJS());
        // A group of item have to init with value
        if (item) {
            const defaultValue = DEFAULT_VALUE[item['widget'] || 'default'];
            if ([':or', ':and', ':not'].includes(item.key)) {
                console.log('Add group');
                item['value'] = [];
            } else {
                item['value'] = defaultValue;
            }
            const newLeft = state.getIn(key).push(Immutable.fromJS({...item}));
            const _state = state.setIn( key, newLeft);
            setState(_state);
            setChange(true);
        }
    }
    function onDeleteItem(key: string[]) {
        // Key end with 'value' is a key of a group
        console.log('onDeleteItem', key, state);
        if (key[key.length-1] === 'value') {
            key.pop();
        }
        const _state = state.deleteIn(key);
        console.log('After', _state);
        setState(_state);
        setChange(true);
    }
    function clearChange() {
        setChange(false);
    }

    let _filter: any = Immutable.fromJS([]);
    let _order: any = Immutable.fromJS([]);

    _state.forEach((item: any, index: any) => {
        const [, op] = item.get('key').split(/[\\!\\:]/);
        if (op === 'sort') {
            item = item.mergeIn([], {path: [index]});
            _order = _order.push(item);
        } else {
            item = item.mergeIn([], {path: [index]});
            _filter = _filter.push(item);
        }
    });

    return [state, [_filter, _order], onChangeItemValue, onDeleteItem, onAddItem, onNegateItemValue, [isChange, clearChange], getCurrentFilter];
};

export const useFilterValue: any = (DEFAULT_FILTER: any = [], callback: any) => {
    /* 
        This hook use to create a new tree for manipulte value of filter
        - Go through the filter value then add path to each item
        - Origin = [
            {":or": [
                {"_id:eq": 1},    
                {"_id:eq": 2},    
            ]}
            {"name:eq":"admin"},
            {"name:sort":1}
        ]
        - Result = [
            {
                key: ":or", 
                path: [0, ':or'],
                value: [
                    {key: "_id:eq", value: 1, path: [0, ':or', 0, '_id:eq']}
                    {key: "_id:eq", value: 2, path: [0, ':or', 1, '_id:eq']}
                ], 
            },
            {key: "name:eq", value: 1, path: [0, 'name:eq']},
            {key: "name:sort", value: 1, path: [0, 'name:sort']},
        ]
        - The modification will apply on the Origin Filter not Result
    */

    DEFAULT_FILTER = DEFAULT_FILTER || [];
    const [state, setState] = useState(Immutable.fromJS(DEFAULT_FILTER));

    useEffect(() => {
        setState(Immutable.fromJS(DEFAULT_FILTER));
    }, [DEFAULT_FILTER]);

    function addPath(data: any, path: string[]) {
        return data.map((element: any, index: string) => {
            if (Immutable.Map.isMap(element)) {
                return addPath(element, [...path, index]);
            } else if (Immutable.List.isList(element)) {
                return addPath(element, [...path, index]);
            } else {
                return Immutable.fromJS({'value': element, path: [...path, index]});
            }
        });
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    function changeFilter(key: Array<string|number>, value: any) {
        const _state = state.setIn([...key], value);
        setState(_state);
        callback(_state);
    }

    function changeOrder(key: string, value: any, replace=true) {
        key = `${key}:sort`;
    
        let _state;

        if (replace) {
            _state = state.filter((item: any) => {
                const key = Object.keys(item.toJS())[0].split(':')[1];
                return key !== 'sort';
            });
            _state = _state.push({[key]: Immutable.fromJS(value)});
        } else {
            const match_index = state.findIndex((item: any) => Object.keys(item.toJS())[0] === key);
    
            if (match_index !== -1) {
                _state = state.setIn([match_index, key], value);
            } else {
                _state = state.push({[key]: Immutable.fromJS(value)});
            }
        }

        setState(_state);
        callback(_state);
    }

    const valueWithPath = addPath(state, []);

    let _filterWithPath: any = Immutable.fromJS([]);
    let _orderWithPath: any = Immutable.fromJS([]);

    if (Immutable.List.isList(valueWithPath)) {
        valueWithPath.forEach((item: any) => {
            item.forEach((element: any, key: any) => {
                const [,op] = key.split(/[\\!\\:]/);
                element = element.set('key', key);
                if (op === 'sort') {
                    _orderWithPath = _orderWithPath.push(element);
                } else {
                    _filterWithPath = _filterWithPath.push(element);
                }
            });
        });
    }

    return [_filterWithPath, _orderWithPath, state, changeOrder];
};
