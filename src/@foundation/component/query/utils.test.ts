import { compileFilter, parseFilter } from './utils';

const Immutable = require('immutable');
const specification = require('./filter.json');

const FILTER_SPEC = [
    {
        'key': 'name:eq',
        'label': 'Name',
        'desc': '',
        'value': 'Thien',
        'widget': 'input:rule',
        'options': {
            'type': 'text'
        }
    },
    {
        'key': ':or',
        'label': 'OR',
        'op': 'OR',
        'desc': '',
        'value': [
            {
                'key': 'email:eq',
                'label': 'Email',
                'desc': '',
                'value': 'marsch.huynh@gmail.com',
                'widget': 'input:rule',
                'options': {
                    'type': 'email'
                }
            },
            {
                'key': 'age:is',
                'label': 'Age',
                'desc': '',
                'value': 47,
                'widget': 'slider:rule',
                'options': {}
            },
            {
                'key': ':and',
                'label': 'AND',
                'op': 'AND',
                'desc': '',
                'value': [
                    {
                        'key': 'age!is',
                        'label': 'Age',
                        'desc': '',
                        'value': 53,
                        'widget': 'slider:rule',
                        'options': {}
                    }
                ],
                'widget': 'group:rule',
                'options': {}
            },
            {
                'key': 'email:eq',
                'label': 'Email',
                'desc': '',
                'value': 'marsch.huynh@gmail.com',
                'widget': 'input:rule',
                'options': {
                    'type': 'email'
                }
            }
        ],
        'widget': 'group:rule',
        'options': {}
    },
    {
        'key': 'address:is',
        'label': 'Address',
        'desc': '',
        'value': '144/72 Nguyen Luong Bang, Lien Chieu, Da Nang',
        'widget': 'input:rule',
        'options': {}
    }
];
const FILTER_EXPECTED = [
    { 'name:eq': 'Thien' },
    {
        ':or': [
            { 'email:eq': 'marsch.huynh@gmail.com' },
            { 'age:is': 47 },
            {
                ':and': [
                    { 'age!is': 53 }
                ]
            },
            { 'email:eq': 'marsch.huynh@gmail.com' }
        ]
    },
    { 'address:is': '144/72 Nguyen Luong Bang, Lien Chieu, Da Nang' }
];

const DATA = [
    { 'name:eq': 'Thien' },
    {
        ':or': [
            { 'email:eq': 'marsch.huynh@gmail.com' },
            { 'age:is': 47 },
            {
                ':and': [
                    { 'age!is': 53 }
                ]
            },
            { 'email:eq': 'marsch.huynh@gmail.com' }
        ]
    },
    { 'address:is': '144/72 Nguyen Luong Bang, Lien Chieu, Da Nang' }
];
const DATA_EXPECTED = [
    {
        'key': 'name:eq',
        'label': 'Name',
        'desc': '',
        'value': 'Thien',
        'widget': 'input:rule',
        'options': {
            'type': 'text'
        }
    },
    {
        'key': ':or',
        'label': 'OR',
        'desc': '',
        'value': [
            {
                'key': 'email:eq',
                'label': 'Email',
                'desc': '',
                'value': 'marsch.huynh@gmail.com',
                'widget': 'input:rule',
                'options': {
                    'type': 'email'
                }
            },
            {
                'key': 'age:is',
                'label': 'Age',
                'desc': '',
                'value': 47,
                'widget': 'slider:rule',
                'options': {}
            },
            {
                'key': ':and',
                'label': 'AND',
                'desc': '',
                'value': [
                    {
                        'key': 'age!is',
                        'label': 'Age',
                        'desc': '',
                        'value': 53,
                        'widget': 'slider:rule',
                        'options': {}
                    }
                ],
                'widget': 'group:rule',
                'options': {}
            },
            {
                'key': 'email:eq',
                'label': 'Email',
                'desc': '',
                'value': 'marsch.huynh@gmail.com',
                'widget': 'input:rule',
                'options': {
                    'type': 'email'
                }
            }
        ],
        'widget': 'group:rule',
        'options': {}
    },
    {
        'key': 'address:is',
        'label': 'Address',
        'desc': '',
        'value': '144/72 Nguyen Luong Bang, Lien Chieu, Da Nang',
        'widget': 'input:rule',
        'options': {}
    }
];

describe('Query Filter', () => {
    it('Transform spec to filter data', () => {
        const spec = compileFilter(Immutable.fromJS(FILTER_SPEC)).toJS();
        expect(spec).toEqual(FILTER_EXPECTED);
    });
    it('Transform filter data to spec', () => {
        const data = parseFilter(specification, Immutable.fromJS(DATA)).toJS();
        expect(data).toEqual(DATA_EXPECTED);
    });
});
