import React from 'react';
import { ENV } from '@foundation/utils';


export default class AppFooter extends React.PureComponent<{siteName: string}, {}, {}> {
    render() {
        const currentYear = (new Date()).getFullYear();

        return (
            <div className="flex bg-white w-full p-sm">
                <span className="flex-grow">Copyright © {currentYear}<strong><a href="/"> {this.props.siteName}</a>.</strong> All rights reserved.</span>
                <span className="text-gray-500">{ENV('VERSION', 'error')}</span>
            </div>
        );
    }
}

export class SimpleFooter extends React.PureComponent {
    render() {
        return (
            <span>Footer</span>
        );
    }
}