import React from 'react';
import { create } from 'react-test-renderer';
import AppFooter from '@foundation/component/footer';


describe('Footer component', () => {
    test('Matches the snapshot', () => {
        const button = create(<AppFooter siteName="ReactJS" />);
        expect(button.toJSON()).toMatchSnapshot();
    });
});