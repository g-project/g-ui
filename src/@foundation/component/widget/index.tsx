import React from 'react';
import { Icon } from 'antd';
import WidgetRegistry from '@foundation/registry/widget';


const Address: React.FC = ({ children }) => {
    return (
        <div>
            <Icon type="profile" />
            {children}
        </div>
    );
};

const Currency: React.FC = ({ children }) => {
    return (
        <div>
            <Icon type="profile" />
            {children}
        </div>
    );
};

const Decimal: React.FC = ({ children }) => {
    return (
        <div className="flex items-center justify-between">
            <Icon type="number" className="text-blue-700"/>
            {children}
        </div>
    );
};

WidgetRegistry.register('global', 'address', Address);
WidgetRegistry.register('global', 'currency', Currency);
WidgetRegistry.register('global', 'decimal', Decimal);

export default { Address, Currency };