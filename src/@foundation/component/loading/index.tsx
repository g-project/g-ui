import React from 'react';
import { ReactComponent as LoadingSVG } from './loading.svg';


function Loading({title, size = 50}: {title?: string, size?: number}) {
    return (
        <div style={{ display: 'flex', flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }}>
            <LoadingSVG style={{ width: size, height: size }} />
            {title && <span>{title}</span>}
        </div>
    );
}

export default Loading;