import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { OAuthProfile } from '@foundation/access';
import { Loading } from '@foundation/component';
import { AuthAction } from 'apps/auth/state';


type PrivateRouteStates = {
    checking: boolean,
    isAuthenticated: boolean,
    error?: string
}

type PrivateRouteProps = typeof AuthAction & {
    component: React.ElementType,
}

class PrivateRoute extends React.Component<PrivateRouteProps, PrivateRouteStates> {
    constructor(props: PrivateRouteProps) {
        super(props);
        this.state = {
            checking: true,
            isAuthenticated: false
        };
    }
    componentDidMount() {
        this.fetchUserInfo();
    }
    fetchUserInfo = async () => {
        try {
            const resp = await OAuthProfile.Get('/user');
            await this._authenticated(resp);
        } catch(e) {
            await this._failed(e);
        }
    }
    _authenticated = async (resp: any) => {
        this.props.authenticate();
        this.props.update_userinfo(resp.data);
        await this.setState({ checking: false, isAuthenticated: true });
    }
    _failed = async (resp: any) => {
        this.props.logout();
        await this.setState({ checking: false, isAuthenticated: false, error: resp.toString() });
    }
    render() {
        const { checking, isAuthenticated } = this.state;
        const Component = this.props.component;
        if (checking) {
            return (
                <Loading title="Authenticating"/>
            );
        }
        return (
            <Route render={(props) => (
                isAuthenticated
                    ? <Component {...props}/>
                    : <Redirect to={{
                        pathname: '/login',
                        state: {
                            from: props.location,
                            error: this.state.error
                        }
                    }}
                    />
            )}
            />
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        isAuthenticated: state.auth.isAuthenticated
    };
};

export default connect(mapStateToProps, AuthAction)(PrivateRoute);
