import React, { useState, useEffect } from 'react';
import Select from 'react-select';

import './style.module.css';

type SelectProps = {
    visible?: boolean,
    autoFocus?: boolean,
    menuIsOpen?: boolean,
    clearAfterSelect?: boolean,
    options: any,
    onSelect?: any
    components?: any,
    formatOptionLabel?: any,
    menuPlacement?: any,
    styles?: any
}

const SelectBox: React.FC<SelectProps> = React.memo(({ options, onSelect, menuIsOpen, components, clearAfterSelect, styles, ...rest }) => {
    const [selectedItem, setSelectedItem] = useState('');
    const [isOpen, setMenuOpen] = useState(menuIsOpen);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        // This code intended to close the selectbox when click an item.
        // @Todos Consider to find another solution
        if (!isOpen) {
            setMenuOpen(true);
        }
    });

    function onHandleSelect(item: any) {
        setSelectedItem(item.value);
        onSelect && onSelect(item.value);
        setMenuOpen(false);
        clearAfterSelect && setSelectedItem('');
    }

    const dataSpec = options.map((item: any) => ({ label: `${item.label}`, value: item }));

    return (
        <div className="flex flex-grow">
            <Select
                styles={{
                    menu: (base) => ({ ...base, margin: '0 0', borderRadius: 0, boxShadow: 'unset', borderColor: 'hsla(0, 0%, 80%, 0.9);', borderWidth: 1, borderTopWidth: 0, borderBottomLeftRadius: 3, borderBottomRightRadius: 3 }),
                    menuList: (base) => ({ ...base, '::-webkit-scrollbar': { width: 8 }, '::-webkit-scrollbar-thumb': { backgroundColor: '#cbd5e064', borderRadius: 10 } }),
                    container: (base) => ({ ...base, display: 'flex', flex: 1, padding: 0 }),
                    control: (base) => ({ ...base, display: 'flex', flex: 1, borderColor: 'hsla(0, 0%, 80%, 0.9);', borderRadius: 3, minHeight: '32px', borderWidth: 1, borderBottomStyle: 'dashed', borderBottomWidth: 1, borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }),
                    indicatorsContainer: (base) => ({ ...base, padding: '0px' }),
                    input: (base, state) => ({ display: 'flex' }),
                    ...styles
                }}
                value={selectedItem as any}
                onChange={onHandleSelect}
                options={dataSpec}
                menuIsOpen={isOpen}
                defaultMenuIsOpen={false}
                components={{ ...components, DropdownIndicator: null, IndicatorSeparator: null }}
                {...rest}
            />
        </div>
    );
});


export default SelectBox;