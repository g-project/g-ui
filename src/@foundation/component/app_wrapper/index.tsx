import React from 'react';
import { Footer, NavBar } from '@foundation/component';
import { SITE_NAME } from 'config';

class AppWrapper extends React.Component {
    render() {
        console.log('SiteWrapper::render');
        return (
            <div style={{ backgroundColor: '#f0f2f5', minHeight: '100vh', display: 'flex', flexDirection: 'column' }}>
                <NavBar />
                <div className="flex flex-col flex-1 flex-grow">
                    {this.props.children}
                </div>
                <Footer siteName={SITE_NAME}/>
            </div>
        );
    }
}

export default AppWrapper;
