import React, { useState, useRef, useEffect } from 'react';

type DropDownProps = {
    overlay: any,
    className?: string,
    replace?: boolean
}

const DropUp: React.FC<DropDownProps> = ({ replace, children, overlay, className }) => {
    const [show, setstate] = useState(false);
    const dropdown = useRef(null);
    const pannel = useRef(null);

    let marginTop = 0;

    if (dropdown && dropdown.current) {
        marginTop = (dropdown.current as any).offsetHeight;
    }
    function handleClickOutside(e: any) {
        const node: any = dropdown.current;
        if (node) {
            if (!node.contains(e.target)) {
                setstate(false);
            }
        }
    }
    useEffect(() => {
        document.addEventListener('click', handleClickOutside);
        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, []);

    function openDropdown(e: React.SyntheticEvent<HTMLDivElement>) {
        setstate(state => !state);
    }

    return (
        <div ref={dropdown} className="flex flex-1 relative">
            <div className="flex flex-col flex-1" onClick={openDropdown}>
                {children}
            </div>
            <div
                ref={pannel}
                className={`flex flex-1 ${show ? 'block' : 'hidden'} absolute z-10 ${className}`}
                style={{ marginTop: replace ? 'unset' : marginTop }}
            >
                {overlay}
            </div>
        </div>
    );
};


const Dropdown: React.FC<DropDownProps> = ({ replace, children, overlay, className }) => {
    const [show, setstate] = useState(false);
    const dropdown = useRef(null);

    let marginTop = 0;

    if (dropdown && dropdown.current) {
        marginTop = (dropdown.current as any).offsetHeight;
    }
    function handleClickOutside(e: any) {
        const node: any = dropdown.current;
        if (node) {
            if (!node.contains(e.target)) {
                setstate(false);
            }
        }
    }
    useEffect(() => {
        document.addEventListener('click', handleClickOutside);
        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, []);

    function openDropdown(e: React.SyntheticEvent<HTMLDivElement>) {
        setstate(state => !state);
    }

    return (
        <div ref={dropdown} className="flex relative">
            <div className="flex flex-col flex-1" onClick={openDropdown}>
                {children}
            </div>
            <div
                className={`${show ? 'block' : 'hidden'} absolute z-10 ${className}`}
                style={{ marginTop: replace ? 'unset' : marginTop }}
            >
                {overlay}
            </div>
        </div>
    );
};

export default Dropdown;
export { DropUp };