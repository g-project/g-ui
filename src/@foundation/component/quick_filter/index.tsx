import React, { useState, useEffect, useRef, useCallback } from 'react';
import { Button, Popover, Input, Icon } from 'antd';
import Select from 'react-select';
import { DataProfile } from '@foundation/access';
import { useLayout } from '@foundation/layout/main/hooks';
import { Loading } from '@foundation/component';

const SaveFilterPopover = ({ value, saveFilter }: any) => {
    const [label, setLabel] = useState('');
    function onSubmit() {
        if (label) {
            saveFilter(label);
        }
    }
    function onChange(e: any) {
        setLabel(e.target.value);
    }
    return (
        <div className="flex flex-col items-start">
            <Input
                value={label}
                onChange={onChange}
                placeholder="Label of filter"
            />
            <Button
                size="small"
                type="primary"
                className="mt-md self-end"
                onClick={onSubmit}
            >
                Submit
            </Button>
        </div>
    );
};

const SelectControl: React.FC<any> = ({ value, label, onChangeOpenMenu, onSaveFilter, ...rest }) => {
    return (
        <div key="1" className="flex items-center flex-1 p-xs">
            <div className="flex flex-grow items-center" onClick={onChangeOpenMenu}>
                <Icon type="star" className="px-sm" />
                <span className="text-base font-medium select-none">Favorite filter</span>
            </div>
            <div className="flex items-center">
                <Popover
                    content={<SaveFilterPopover value={value} saveFilter={onSaveFilter} />}
                    title="Save current filter"
                    placement="top"
                    trigger="click"
                    overlayClassName="save-filter"
                >
                    <Icon type="save" className="text-xl text-blue-600" />
                </Popover>
            </div>
        </div>
    );
};

const QuickFilter = ({ filterValue, applyFilter, onClick }: any) => {
    const [, applyParam] = useLayout();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [reload, setReload] = useState(false);
    const [isMenuOpen, setOpenMenu] = useState(false);

    const container = useRef(null);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const handleClickOutside = (e: any) => {
        const node: any = container.current;
        if (node) {
            if (!node.contains(e.target)) {
                setOpenMenu(false);
            }
        }
    };

    useEffect(() => {
        setLoading(true);
        let unMount = false;
        DataProfile.Get('/filter')
            .then((data: any) => {
                if (!unMount) {
                    setData(data.data);
                    setLoading(false);
                }
            }).catch((e: any) => {
                setLoading(false);
            });
        return () => {
            unMount = true;
        };
    }, [reload]);

    useEffect(() => {
        document.addEventListener('click', handleClickOutside);
        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, [handleClickOutside]);

    async function onSaveFilter(name: string) {
        try {
            await DataProfile.Post('/filter', {
                data: {
                    data: JSON.stringify(filterValue),
                    label: name
                },
            });
            setReload(state => !state);
        } catch (e) {
            console.log('SaveFilter error', e);
        }
    }

    const onToggleMenu = useCallback(() => {
        setOpenMenu(open => !open);
    }, []);

    const onChangeValue = (data: any) => {
        applyParam('q', { filter: JSON.parse(data.value.data) });
        setOpenMenu(false);
    };

    if (loading) {
        return (
            <div style={{ display: 'flex', flex: '2 2 0', width: 250 }}>
                <Loading />
            </div>
        );
    }
    const dataSpec = data.map((item: any) => ({ label: `${item.label}`, value: item }));
    return (
        <div ref={container} className="flex flex-col justify-between bg-primary sticky bottom-0">
            <Select
                onChange={onChangeValue}
                options={dataSpec}
                menuPlacement='top'
                menuIsOpen={isMenuOpen}
                components={{ Control: ({ ...rest }: any) => <SelectControl onSaveFilter={onSaveFilter} onChangeOpenMenu={onToggleMenu} />, DropdownIndicator: null, IndicatorSeparator: null }}
                styles={{
                    menu: (base: any) => ({
                        ...base,
                        margin: '0 0',
                        borderRadius: 0,
                        boxShadow: 'unset',
                        borderColor: 'hsla(0, 0%, 80%, 0.9);',
                        borderWidth: 1,
                        borderBottomWidth: 0,
                        borderTopLeftRadius: 2,
                        borderTopRightRadius: 2
                    }),
                    menuList: (base: any) => ({
                        ...base,
                        '::-webkit-scrollbar': { width: 8 },
                        '::-webkit-scrollbar-thumb': { backgroundColor: '#cbd5e064', borderRadius: 10 }
                    }),
                    container: (base: any) => ({ ...base, display: 'flex', flex: 1, padding: 0 }),
                    control: (base: any) => ({
                        ...base,
                        display: 'flex',
                        flex: 1,
                        borderColor: 'hsla(0, 0%, 80%, 0.9);',
                        borderRadius: 3,
                        minHeight: '32px',
                        borderWidth: 1,
                        borderBottomWidth: 1,
                    }),
                    indicatorsContainer: (base: any) => ({ ...base, padding: '0px' }),
                    input: (base: any, state: any) => ({ display: 'flex' }),
                }}
            />
        </div>
    );
};

export default QuickFilter;