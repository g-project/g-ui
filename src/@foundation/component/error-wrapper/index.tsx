import React from 'react';
import { Result, Icon, Button } from 'antd';


export default class ErrorBoundary extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch(error: any, info: any) {
        // Display fallback UI
        console.log('Error', JSON.stringify(info));
        // You can also log the error to an error reporting service
        this.setState({ hasError: true, error, errorInfo: JSON.stringify(info)});
        // logErrorToMyService(error, info);
    }

    render() {
        if (this.state.hasError) {
            const { error, errorInfo } = this.state;
            // You can render any custom fallback UI
            return (
                <Result
                    status="error"
                    icon={<Icon type="warning" />}
                    title={'Sorry, something wrong.'}
                    subTitle={'Please contact to Administrator.'}
                    extra={
                        <Button size="large" type="primary">
                            <a href={`mailto:admin@abx-ltd.com?subject=[PCPD] ${error}&body=${errorInfo}`} target="_top" style={{fontWeight: 'bold'}}>Send</a>
                        </Button>
                    }
                />
            );
        }
        return this.props.children;
    }
}