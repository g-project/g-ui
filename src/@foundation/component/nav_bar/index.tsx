import React, { useState } from 'react';
import { LOGOUT_URL, SITE_NAME, APP_NAME, LOGO_URL } from 'config';
import Icon from 'antd/lib/icon';
import { Divider, Avatar } from 'antd';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import SimpleDropdown from '@foundation/component/dropdown';
import { PROFILE_URL } from 'config';

import { flatten } from './utils';
import style from './style.module.scss';
import MenuWrap from '../menu';

function NameInitials(name: string) {
    return name && name.split(/\s+/).map((s: string) => s[0]).join('');
}

type UserInfo = {
    email: string,
    full_name: string
}

function MenuOverlay({ userInfo }: { userInfo: UserInfo }) {
    return (
        <div className="mx-auto flex flex-col bg-white rounded shadow overflow-hidden">
            <div className="flex p-2 items-center hover:bg-blue-100">
                <Avatar size="small" style={{ marginRight: 10 }}>U</Avatar>
                <span>{userInfo && userInfo.email}</span>
            </div>
            <Divider dashed style={{ margin: '0px 0px' }} />
            <a href={PROFILE_URL} className="flex p-3 items-center hover:bg-blue-100">
                <Icon className="mr-2" type="user" />
                Profile
            </a>
            <Link to="/setting" className="flex p-3 items-center hover:bg-blue-100">
                <Icon className="mr-2" type="setting" />
                Setting
            </Link>
            <Divider dashed style={{ margin: '0px 0px' }} />
            <div className="flex p-3 items-center hover:bg-blue-100">
                <Icon className="mr-2" type="logout" />
                <a href={LOGOUT_URL}>Logout</a>
            </div>
        </div>
    );
};

export default function NavBar({ menu: NAV }: any): JSX.Element {
    console.log('NAV', NAV);
    const FLATTEN_NAV = NAV && flatten(NAV);
    console.log('FLATTEN_NAV', FLATTEN_NAV);

    const [isShow, showMenu] = useState(false);
    const userInfo = useSelector((state: any) => state.auth.userInfo);
    const isAuthenticated = useSelector((state: any) => state.auth.isAuthenticated);
    return (
        <div className={`${isShow && style.navOpen} ${style.navBar}`}>
            <div className={style.branchName}>
                <div className="flex items-center bg-primary px-md">
                    <Avatar
                        size="large"
                        className="mr-md"
                        src={LOGO_URL}
                    />
                    <Link to="/">
                        <div className="flex flex-col items-start sm:items-start w-auto">
                            <span className="text-lg text-on-primary">{SITE_NAME}</span>
                            <span className="text-xs text-on-primary">{APP_NAME}</span>
                        </div>
                    </Link>
                </div>
                <Icon onClick={() => showMenu(state => !state)} type="menu" className={`${style.menuToggle}`} />
            </div>
            {isAuthenticated && NAV && <MenuWrap menuFlatten={FLATTEN_NAV} data={NAV} />}
            <nav className={`${style.navItemGroup} ${style.navItemRight}`}>
                {/* <div className={`${style.navItem} ${style.navSearchItem}`}>
                    <Search
                        placeholder="Site search ..."
                        onSearch={value => console.log(value)}
                    />
                </div> */}
                {isAuthenticated ?
                    (<SimpleDropdown
                        overlay={<MenuOverlay userInfo={userInfo} />}
                        className="right-0 z-50 pt-xs"
                    >
                        <div className="flex flex-grow items-center cursor-pointer hover:bg-second px-sm">
                            <Avatar className="select-none">{NameInitials(userInfo.full_name)}</Avatar>
                            <span className="select-none">&nbsp;{userInfo.full_name}</span>
                        </div>
                    </SimpleDropdown>)
                    :
                    (
                        <Link className={style.navItem} to={'/login'}>Login</Link>
                    )
                }
            </nav>
        </div>
    );
}
