type MenuItem = {
    title: string,
    key: string,
    link: string,
    child?: Array<MenuItem>
}

function flatten(data: Array<MenuItem>, result: any = {}, rootkey: string = '') {
    for (let _item of data) {
        // Prevent side effect
        const item = { ..._item };
        const key = rootkey ? [rootkey, item.key].join('.') : item.key;

        // Override the old key
        _item.key = key;

        if (item.child) {
            flatten(item.child, result, key);
        }
        delete (item.child);
        result[key] = { ...item, key };
    }
    return result;
}

export { flatten };
