import React from 'react';
import { ItemTable } from './type';

const SimpleTabelItem: React.FC<ItemTable> = ({ data, headers, setActiveItem, active }) => {
    return (
        <tr className={`bg-white ${active && 'bg-blue-500'}`} onClick={() => setActiveItem(data)}>
            {headers.map((key: string) => {
                const value = typeof data[key] !== 'object' ? data[key] : '';
                return (<td key={key}>{value}</td>);
            })}
        </tr>
    );
};

export { SimpleTabelItem };
