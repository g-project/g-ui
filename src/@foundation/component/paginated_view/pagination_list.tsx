import React from 'react';
import DeepEqual from 'deep-equal';
import { OfflineBundle } from '@foundation/access/bundle';
import { Pagination, Button } from 'antd';

import { ReactListProps, ReactListStates, LBaseProps } from './type';
import PaginationService, { Query, DataBundle } from './services/pagination';
import { PaginatorOptionDefault } from './const';
import Loading from '@foundation/component/loading';
import BatchService from './services/multiselect';

/* ReactListBase
*/
export default class ReactListBase<
    T = {},
    P extends LBaseProps<T> = ReactListProps<T>,
    S extends ReactListStates<T> = ReactListStates<T>
    >
    extends React.Component<P, S> {

    __data: Array<T> = []
    __selectedItem: Record<string, T> = {}

    __error: string | null

    paginationService: PaginationService<T>
    batchService: BatchService<T>

    constructor(props: P) {
        super(props);
        this.__error = null;
        this.__selectedItem = {};

        this.fetch = this.fetch.bind(this);

        this.paginationService = new PaginationService(this.fetch);
        this.batchService = new BatchService();

        props.pageSize && this.paginationService.setPageSize(props.pageSize);
        props.filter && this.PS.setFilter(props.filter);

        this.state = {
            activeItem: null,
            data: [] as Array<T>,
            error: null,
            filter: props.filter || {} as Record<string, any>,
            filterKey: '',
            loading: false,
            sortKey: {} as Record<string, number>,
            batchMode: props.batchMode,
            selectedItem: {},
        } as S;
    }
    static getDerivedStateFromProps(props: ReactListProps<any>, state: any) {
        const newState = {
            filter: props.filter,
        };
        return newState;
    }
    componentDidUpdate(prevProps: P) {
        // Handle re-fetch data when filter change
        const equal = DeepEqual(prevProps.filter, this.props.filter);
        if (!equal && this.props.filter) {
            this.PS.setFilter(this.props.filter as any);
            this.refresh();
        }
    }
    componentDidMount() {
        this.refresh();
    }
    fetch(query: Query): Promise<DataBundle<T>> {
        // This function define how to get data
        if (this.props.data) {
            return OfflineBundle(this.props.data, query);
        }
        throw Error('Data props it not exist. Please override method fetch.');
    }
    get identifier(): string | null {
        // Extract the identification key of item
        throw Error('Not implemented.');
    }
    get PS() {
        return this.paginationService;
    }
    get activeItem() {
        return this.state.activeItem;
    }
    extractKey(item: any) {
        // Return the identification of item
        if (!item) {
            return Math.random();
        }
        if (this.props.extractKey) {
            return this.props.extractKey(item);
        }
        return this.identifier && item[this.identifier];
    }
    setData(data: any) {
        this.__data = data;
        this.setState({ loading: false, data });
    }
    setError(e: string | null) {
        this.__error = e;
        this.setState({ loading: false, error: e });
    }
    next = () => {
        this.PS.nextPage();
        this.refresh();
    }
    previous = () => {
        this.PS.previousPage();
        this.refresh();
    }
    setPageSize = (value: number) => {
        this.PS.setPageSize(value);
        this.refresh();
    }
    setPage = (value: number) => {
        this.PS.setPage(value);
        this.refresh();
    }
    refresh = async () => {
        await this.setState({ loading: true });
        try {
            await this.PS.refresh();
            this.setData(this.PS.data);
            this.state.error && this.setError(null);
        } catch (e) {
            this.setError(e);
            console.log('Refresh data error', e);
        }
    }
    setActiveItem = (data: any) => {
        if (this.props.onItemClick) {
            this.props.onItemClick(data);
        }
        this.setState((prev: ReactListStates<T>) => {
            const item = this.extractKey(prev.activeItem) === this.extractKey(data) ? null : data;
            return {
                activeItem: item
            };
        });
    }
    selectItem = (data: any) => {
        const id = this.extractKey(data);
        this.batchService.toogle(id, data);
        this.setState({
            selectedItem: this.batchService.getData()
        });
    }
    clearItem = (data: any) => {
        this.batchService.clear();
        this.setState({
            selectedItem: this.batchService.getData()
        });
    }
    onChangePage = (page: number, pageSize?: number) => {
        this.setPage(page);
    }
    onShowSizeChange = (current: number, size: number) => {
        this.setPageSize(size);
    }
    sort = (key: string) => {
        console.log('Not implemented.');
    }
    renderContent = () => {
        const { data, activeItem } = this.state;
        const Item = this.props.Item;
        return data.map((item: T, index: number) => {
            const active = this.extractKey(activeItem) === this.extractKey(item);
            const selected = this.extractKey(item) in this.state.selectedItem;
            return (<Item
                key={this.extractKey(item)}
                active={active}
                selected={selected}
                data={item}
                index={index}
                setActiveItem={this.setActiveItem}
                selectItem={this.selectItem}
            />);
        });
    }
    renderPaginator = (position: string) => {
        const { paginatorOption = PaginatorOptionDefault } = this.props;
        const props = {
            size: 'small',
            current: this.PS.page,
            onChange: this.onChangePage,
            onShowSizeChange: this.onShowSizeChange,
            showSizeChanger: true,
            total: this.PS.total,
            pageSize: this.PS.pageSize,
            pageSizeOptions: ['5', '10', '25', '50', '100', '200'],
        };
        switch (paginatorOption.mode) {
        case 'simple':
            props['simple'] = true;
            break;
        case 'full':
            props['showTotal'] = (total: number, range: number[]) => {
                return `${range[0]}-${range[1]} of ${total} items`;
            };
            break;
        }
        return (
            <div key={`pagitor-${position}`} className="flex justify-end p-sm bg-gray-200 border-b border-l border-r z-0">
                <Pagination {...props} />
            </div>
        );
    }
    renderHeader() {
        return null as unknown as JSX.Element;
    }
    renderBatchTool() {
        const count = Object.keys(this.state.selectedItem).length;
        if (this.props.batchMode && count > 0) {
            return (
                <div  className="flex items-center bg-blue-600 p-sm sticky bottom-0 rounded-sm">
                    <Button size="small" type="primary" className="bg-red-500 border-0" onClick={this.clearItem}>Clear</Button>
                    <span className="ml-auto text-white">{count} selected item(s)</span>
                </div>
            );
        }
        return null;
    }
    render() {
        const { paginatorOption = PaginatorOptionDefault } = this.props;
        const { loading } = this.state;
        const items = [this.renderHeader(), this.renderContent(), this.renderBatchTool()];
        switch (paginatorOption.position) {
        case 'top':
            items.unshift(this.renderPaginator('top'));
            break;
        case 'bottom':
            items.push(this.renderPaginator('top'));
            break;
        case 'both':
            items.unshift(this.renderPaginator('top'));
            items.push(this.renderPaginator('top'));
            break;
        default:
        }
        return (
            <div className="w-full">
                <div className="flex flex-col flex-1 relative">
                    {loading && <div className="flex absolute w-full h-full" style={{ backgroundColor: '#00000045' }}>
                        <Loading />
                    </div>}
                    {items}
                </div>
            </div>
        );
    }
}