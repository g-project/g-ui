export declare type PaginatorOption = {
    position?: 'both' | 'top' | 'bottom',
    mode?: 'simple' | 'normal' | 'full'
}

// Item type
export declare type ItemList = {
    active:boolean,
    data: T,
    index: number,
    setActiveItem: (data: T) => void
}

export declare type ItemTable = ItemList & {
    headers: Array<string>,
}

export declare type PCPDItemTable = ItemTable & {
    fields: Record<string, string>,
    batchMode: boolean,
    selected: boolean,
    selectItem: (data: any) => void,
    contentLayout?: any
}

// Props type
export declare type LBaseProps<T> = {
    extractKey?: (item: any) => any
    filter?: Record<string, any>
    data?: any
    Item?: any
    onChangeFilter?: (path: string[], value: any) => void
    pageSize?: number
    paginatorOption?: PaginatorOption = { position: 'bottom', mode: 'normal' }
    textSearchKey?: string
    title?: string
    onItemClick?: (item: any) => void
    batchMode?: boolean
}

export declare type ReactListProps<T> = LBaseProps<T> & {
    data?: Array<T>
    Item?: React.FC<ItemList>
}

export declare type TableBaseProps<T> = LBaseProps<T> & {
    data?: Array<T>
    Item?: React.FC<ItemTable>
    sort?: any
    headers?: Array<string>,
    contentLayout?: any
}

export declare type PCPDTableProps<T> = LBaseProps<T> & {
    data?: Array<T>
    Item?: React.FC<PCPDItemTable>
    source: string
    changeOrder?: any
    sort?: any
    headers?: Array<string>
    contentLayout?: any
}


// State type
export interface ReactListStates<T> {
    activeItem: T | null
    data: Array<T>
    selectedItem: Record<string, T>
    error: string | null
    filter: Record<string, any>
    filterKey: string
    loading: boolean
    batchMode: boolean
    sortKey: Record<string, number>
}

export declare type TableBaseStates<T> = ReactListStates<T> & {
    filter: List<any>
    loading: boolean
    sortKey: Record<string, number>
}

