import React from 'react';
import DeepEqual from 'deep-equal';
import ReactPaginationListBase from './pagination_list';
import { TableBaseProps, TableBaseStates } from './type';
import { SimpleTabelItem } from './simple_row';
import { Icon } from 'antd';

const transformSortKey = (sort: any) => {
    const sortKey = {};
    Object.keys(sort).map(item => {
        const key = sort[item].key.split(':')[0];
        sortKey[key] = sort[item].value;
        return item;
    });
    return sortKey;
};

export default class ReactTableBase<
    T = {},
    P extends TableBaseProps<T> = TableBaseProps<T>,
    S extends TableBaseStates<T> = TableBaseStates<T>
    > extends ReactPaginationListBase<T, P, S> {

    // Override
    static getDerivedStateFromProps(props: TableBaseProps<any>, state: any) {
        const newState = ReactPaginationListBase.getDerivedStateFromProps(props, state);
        if (props.sort) {
            const sort = props.sort.toJS() || {};
            newState['sortKey'] = transformSortKey(sort);
        }
        return newState;
    }
    // Override
    get header(): Array<string> {
        const { headers } = this.props;
        return (headers || []) as Array<string>;
    }
    // Layout of a row
    get contentLayout() {
        if (this.props.contentLayout) {
            return this.props.contentLayout;
        }
        return [new Array(this.header.length).fill(1)];
    }
    // Override
    componentDidUpdate(prevProps: TableBaseProps<T>) {
        // Handle re-fetch data when filter change
        const equal = DeepEqual(prevProps.filter, this.props.filter) && DeepEqual(prevProps.sort, this.props.sort);
        if (!equal && this.props.filter) {
            this.props.filter && this.PS.setFilter(this.props.filter as any);
            this.refresh();
        }
    }
    // Override
    renderHeader() {
        return (
            <div></div>
        );
    }
    // Override
    renderRowContent() {
        const { data, activeItem } = this.state;
        const Item = this.props.Item || SimpleTabelItem;
        const row_header = this.header;
        return data.map((item: T, index: number) => {
            const active = this.extractKey(activeItem) === this.extractKey(item);
            const selected = this.extractKey(item) in this.state.selectedItem;
            return (<Item
                active={active}
                selected={selected}
                data={item}
                headers={row_header}
                index={index}
                key={`${this.extractKey(item)}-${index}`}
                setActiveItem={this.setActiveItem}
                selectItem={this.selectItem}
            />);
        });
    }
    // Override
    renderRowHeader() {
        const { sortKey } = this.state;
        return (
            <tr>
                {this.header.map((item: string) => {
                    return (<td onClick={() => this.sort(item)} key={item}>
                        {item}
                        <Icon
                            className={sortKey[item] ? 'text-red-700' : 'text-transparent'}
                            type={sortKey[item] === 1 ? 'sort-ascending' : 'sort-descending'}
                        />
                    </td>);
                })}
            </tr>
        );
    }
    // Override
    renderContent: any = () => {
        return (
            <table key="table" className="w-full bg-second border">
                <thead>
                    {this.renderRowHeader()}
                </thead>
                <tbody>
                    {this.renderRowContent()}
                </tbody>
            </table>
        );
    }
}
