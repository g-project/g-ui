
export default class BatchService<T> {
    __data: Record<string, T>

    constructor() {
        this.__data = {};
    }
    setData(data: Record<string, T>) {
        this.__data = data;
    }
    getData() {
        return this.__data;
    }
    addItem(id: string, data: T) {
        this.__data[id] = data;
    }
    removeItem(id: string) {
        delete(this.__data[id]);
    }
    toogle(id: string, data: T) {
        if (id in this.__data) {
            this.removeItem(id);
        } else {
            this.addItem(id, data);
        }
    }
    clear() {
        this.__data = {};
    }
}