import DEFAULT from './const.json';

export type Pagination = {
    page: number,
    max_results: number,
    total: number,
}

export type Query = {
    filter: Record<string, any>,
    pagination: Pagination,
    metadata?: Metadata | null
}

export type Metadata = {
    title: string,
    identifier: string | null,
    filters:  Record<string, any>,
    orders: Array<string>,
    fields: Record<string, any>
}

export type Filter = Record<string, any>

export type DataBundle<T> = {
    _items: T[],
    _metadata?: Metadata
    _pagination: Pagination
}

export interface FetcherType<T> {
    (query: Query): Promise<DataBundle<T>>
}

export default class PaginationService<T> {
    __query: Query
    __data: Array<T> = []
    __metadata: Metadata
    __selectedItems: Array<T> = []
    __activeItems: T | undefined
    __fetching: Boolean
    __pagination: Pagination
    __filter: Filter
    __fetcher: FetcherType<T>

    constructor(fetcher: FetcherType<T>) {
        this.__fetching = false;
        this.__pagination = {max_results: DEFAULT.MAX_RESULTS, page: DEFAULT.PAGE, total: 0};
        this.__filter = {};
        this.__metadata = {filters: [], orders: [], fields: [], title: '', identifier: null};
        this.__query = {
            filter: this.__filter,
            pagination: this.__pagination,
            metadata: null
        };
        this.__fetcher = fetcher;
    }
    get data() {
        return this.__data;
    }
    get page() {
        return this.__pagination.page;
    }
    get pageSize() {
        return this.__pagination.max_results;
    }
    get total() {
        return this.__pagination.total;
    }
    get query(): Query {
        return {
            pagination: this.__pagination,
            filter: this.__filter,
        };
    }
    get fields() {
        return (this.__metadata && this.__metadata['fields']) || [];
    }
    get metadata() {
        return this.__metadata || null;
    }
    _setPagination(meta: Pagination) {
        this.__pagination = {
            ...this.__pagination,
            page: Number(meta.page) ? Number(meta.page) : 1,
            total: Number(meta.total) ? Number(meta.total) : 0
        };
    }
    _setData(data: Array<T>) {
        this.__data = data;
    }
    _setMetaData(data: Metadata) {
        this.__metadata = data;
    }
    _fetchData() {
        // Begin fetching process
        const query = {
            pagination: this.__pagination,
            filter: this.__filter
        };
        return this.__fetcher(query);
    }
    _setResponse(response: any) {
        // Finish fetching process
        this._setData(response['_items']);
        this._setPagination(response['_pagination']);
        this._setMetaData(response['_metadata']);
        this.__fetching = false;
    }
    nextPage() {
        this.__pagination = {
            ...this.__pagination,
            page: this.__pagination.page + 1
        };
    }
    previousPage() {
        this.__pagination = {
            ...this.__pagination,
            page: this.__pagination.page === 1 ? 1 : this.__pagination.page - 1
        };
    }
    setPage(value: number) {
        this.__pagination = {
            ...this.__pagination,
            page: value
        };
    }
    setPageSize(value: number) {
        this.__pagination = {
            ...this.__pagination,
            page: 1,
            max_results: value
        };
    }
    setFilter(filter: Filter) {
        this.__filter = filter;
    }
    refresh = async () => {
        // Fetching process
        const bundle = await this._fetchData();
        this._setResponse(bundle);
    }
}