export type WidgetType = {
    title?: string,
    secure?: boolean,
    exact?: boolean,
    component: React.ElementType
}

export default class WidgetRegistry {
    private static INSTANCE: {[x: string]: WidgetType} = {}

    static init(name: string) {
        WidgetRegistry.INSTANCE[name] = {} as WidgetType;
    }

    static register(module: string = 'global', key: string, config: React.ReactType) {
        if (!(module in WidgetRegistry.INSTANCE)) {
            WidgetRegistry.INSTANCE[module] = {} as WidgetType;    
        }
        WidgetRegistry.INSTANCE[module][key] = config;
    }

    static get(module: string = 'global', key: string): React.ReactType {
        if (module in WidgetRegistry.INSTANCE) {
            return WidgetRegistry.INSTANCE[module][key];
        }
        throw Error('Widget not found');
    }

    static get ALL() {
        let result = {};
        for (let key in WidgetRegistry.INSTANCE) {
            result = Object.assign({}, result, WidgetRegistry.INSTANCE[key]);
        }
        return result;
    }
}
