export type StateModuleType<T, U> = {
    reducer: T,
    action: U
}

export default class StateRegistry {
    private static REDUCER: DictType = {}
    private static ACTION: DictType = {}

    static init(name: string) {
        StateRegistry.REDUCER[name] = {} as DictType;
        StateRegistry.ACTION[name] = {} as DictType;
    }

    static register<T, U>(module: string, state: StateModuleType<T, U>) {
        StateRegistry.REDUCER[module] = state.reducer;
        StateRegistry.ACTION[module] = state.action;
    }

    static get(module: string) {
        return {
            reducer: StateRegistry.REDUCER[module],
            action: StateRegistry.ACTION[module]
        };
    }

    static get ALL_REDUCER() {
        return StateRegistry.REDUCER;
    }

    static get ALL_ACTION() {
        return StateRegistry.ACTION;
    }
}
