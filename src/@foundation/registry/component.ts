export type ComponentType = {
    title?: string,
    secure?: boolean,
    exact?: boolean,
    component: React.ElementType
}

export default class ComponentRegistry {
    private static INSTANCE: {[x: string]: ComponentType} = {}

    static init(name: string) {
        ComponentRegistry.INSTANCE[name] = {} as ComponentType;
    }

    static register(module: string, key: string, config: ComponentType) {
        ComponentRegistry.INSTANCE[module][key] = config;
    }

    static get(module: string, key: string): ComponentType | JSX.Element {
        return ComponentRegistry.INSTANCE[module][key];
    }

    static get ALL() {
        let result = {};
        for (let key in ComponentRegistry.INSTANCE) {
            result = Object.assign({}, result, ComponentRegistry.INSTANCE[key]);
        }
        return result;
    }
}
