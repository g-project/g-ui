export type RouteModuleType = {
    title?: string,
    secure?: boolean,
    exact?: boolean,
    component: React.ElementType
}

export default class RouteRegistry {
    private static INSTANCE: {[x: string]: RouteModuleType} = {}

    static init(name: string) {
        RouteRegistry.INSTANCE[name] = {} as RouteModuleType;
    }

    static register(module: string, path: string, config: RouteModuleType) {
        RouteRegistry.INSTANCE[module][path] = {...config, path};
    }

    static get(module: string, path: string): RouteModuleType {
        return RouteRegistry.INSTANCE[module][path];
    }

    static get ALL() {
        let result = {};
        for (let key in RouteRegistry.INSTANCE) {
            result = Object.assign({}, result, RouteRegistry.INSTANCE[key]);
        }
        return result;
    }
}
