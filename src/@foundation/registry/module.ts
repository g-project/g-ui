import StateRegistry, { StateModuleType } from './state';
import RouteRegistry, { RouteModuleType } from './route';
import ComponentRegistry from './component';
import WidgetRegistry from './widget';

export default class Module {
    name: string
    static INSTANCE: string[] = []

    constructor(name: string) {
        this.name = name;
        if (name in Module.INSTANCE) {
            throw Error(`Module [${module}] already existed.`);
        }
        Module.INSTANCE.push(name);

        ComponentRegistry.init(name);
        RouteRegistry.init(name);
        StateRegistry.init(name);
        WidgetRegistry.init(name);
    }

    state<R, A>(state: StateModuleType<R, A>) {
        StateRegistry.register(this.name, state);
    }

    route(path: string, config: RouteModuleType) {
        RouteRegistry.register(this.name, path, config);
    }

    component(key: string, config: any) {
        ComponentRegistry.register(this.name, key, config);
    }
}
