export {default as WidgetRegistry} from './widget';
export {default as StateRegistry} from './state';
export {default as RouteRegistry} from './route';
export {default as ComponentRegistry} from './component';