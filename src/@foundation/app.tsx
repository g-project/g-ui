import React from 'react';
import { createBrowserHistory } from 'history';
import { Route } from 'react-router';

import PrivateRoute from './component/private_route';
import configStoreAsync from './redux/configStoreAsync';
import AppModule from './registry/module';
import RouteRegistry from './registry/route';


type RootApplicationProps = {
    loading: boolean;
}

export default class BaseApplication extends React.Component<{}, RootApplicationProps> {
    store: any
    history: any
    state = {
        loading: true
    }
    getInstalledModule(): any {
        throw Error('Not implemented');
    }
    componentDidMount() {
        this.initiate();
    }
    setupModule() {
        const modules = this.getInstalledModule();
        for (let key in modules) {
            const module = new AppModule(key);
            modules[key].setup(module);
        }
    }
    initiate = async () => {
        await this.setState({ loading: true });

        // Setup module
        this.setupModule();

        // Setup redux state
        this.history = createBrowserHistory();
        this.store = await configStoreAsync(this.history);

        await this.setState({ loading: false });
    }
    renderRoute() {
        const allRoute = RouteRegistry.ALL;
        return Object.keys(allRoute).map(path => {
            const route = allRoute[path];
            const { secure = true, ...props } = route;
            const Comp = secure ? PrivateRoute : Route;
            return (
                <Comp key={path} path={path} {...props} />
            );
        });
    }
};
