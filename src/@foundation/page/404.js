import React from 'react';

export default class Error404 extends React.PureComponent {
    render() {
        return (
            <div style={{ display: 'flex', justifyContent: 'center' }} >
                <div style={{ position: 'relative' }}>
                    <span style={{ fontSize: '25vw', color: 'rgb(217, 220, 226)'}}>404</span>
                    <span style={{ fontSize: '4vw', position: 'absolute', top: '43%', right: 0, left: 0, textAlign: 'center', color: '#6e7c90' }}>
                        PAGE NOT FOUND
                    </span>
                </div>
            </div>

        );
    }
}
