import React, { Component } from 'react';
import { Tabs, Switch, Select } from 'antd';

const { TabPane } = Tabs;

const { Option } = Select;

export default class Setting extends Component {
    render() {
        return (
            <div className="container p-md">
                <Tabs defaultActiveKey="1" tabPosition='top'>
                    <TabPane tab="Application" key="1">
                        Language:
                        <Select
                            style={{ marginLeft: 12, width: 200 }}
                            placeholder="Select your language"
                            optionFilterProp="children"
                        >
                            <Option value="en">English</Option>
                            <Option value="vn">Vietnamese</Option>
                            <Option value="cn">Chinese</Option>
                        </Select>
                    </TabPane>
                    <TabPane tab="Appearance" key="2">
                        Dark mode:
                        <Switch style={{ marginLeft: 12 }} defaultChecked />
                    </TabPane>
                    <TabPane tab="Privacy" key="3">
                        Allow cookies:
                        <Switch style={{ marginLeft: 12 }} defaultChecked />
                    </TabPane>
                </Tabs>
            </div>
        );
    }
}
