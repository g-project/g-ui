import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { DataProfile } from '@foundation/access';
import { useDispatch } from 'react-redux';
import { AUTHENTICATE, UPDATE_USER_INFO } from '../../state/auth/const';
import { useHistory } from 'react-router';

const NormalLoginForm = (props: any) => {
    const dispatch = useDispatch();
    const history = useHistory();

    console.log(history);

    const onSubmit = (event: any) => {
        console.log(event);
        event.preventDefault();
        props.form.validateFields(async (err: any, values: any) => {
            if (!err) {
                try {
                    const data = await DataProfile.Post('/login', { data: values });
                    await dispatch({ type: UPDATE_USER_INFO, payload: { email: data.data.email, full_name: data.data.username, user_id: data.data.id, user_city: data.data.user_city, recommend: data.data.recommend } });
                    await dispatch({ type: AUTHENTICATE });
                    if (data.status === 200) {
                        history.location.state ? history.push(history.location.state) : history.push('/');
                    }
                } catch (e) {
                    console.log('Submit error', e);
                }
            }
        });
    };

    const { getFieldDecorator } = props.form;

    return (
        <div className='login-card flex-center'>
            <div className='flex p-5 flex-col items-center'>
                <img src={require('./login.jpg')} alt='login'/>
                <a href="/auth/register">Create an account</a>
            </div>
            <div className="flex-center flex-col">
                <Form onSubmit={onSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'Please input your username!' }],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="Username"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Please input your Password!' }],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                placeholder="Password"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(<Checkbox>Remember me</Checkbox>)}
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );

};

const WrappedNormalLoginForm: any = Form.create({ name: 'normal_login' })(NormalLoginForm);

export default WrappedNormalLoginForm;