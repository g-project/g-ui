import React, { useState } from 'react';
import {
    Form,
    Input,
    Button,
} from 'antd';

import '../styles.scss';
import { DataProfile } from 'apps/base/access';
import { useDispatch } from 'react-redux';
import { AUTHENTICATE, UPDATE_USER_INFO } from '../../state/auth/const';
import { useHistory } from 'react-router';
import { ADD_CITYID } from '../../state/search/const';
import Selector from 'apps/blank/component/selector';
import { user_city } from 'apps/data';

const RegistrationForm = (props: any) => {
    const dispatch = useDispatch();

    const history = useHistory();
    const [confirmDirty, setConfirmDirty] = useState(false);
    const { getFieldDecorator } = props.form;

    const handleSubmit = (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        props.form.validateFieldsAndScroll(async (err: any, values: any) => {
            if (!err) {
                try {
                    const data = await DataProfile.Post('/register', { data: values });
                    console.log('data_get', data);
                    if (data.status === 200) {
                        const data_get = await DataProfile.Get(`/user/${data.data.id}`);
                        console.log('data_get', data_get);
                        await dispatch({ type: UPDATE_USER_INFO, payload: { email: data_get.data.email, full_name: data_get.data.username, user_id: data_get.data._id, user_city: data_get.data.address } });
                        await dispatch({ type: ADD_CITYID, payload: '5dcc04ec68b6074c50ba01fd' });
                        await dispatch({ type: AUTHENTICATE });
                        history.location.state ? history.push(history.location.state) : history.push('/search');
                    }
                } catch (e) {
                    console.log('Submit error', e);
                }
            }
        });
    };

    const handleConfirmBlur = (e: { target: { value: any; }; }) => {
        const { value } = e.target;
        setConfirmDirty(confirmDirty || !!value);
    };

    const compareToFirstPassword = (rule: any, value: any, callback: { (arg0: string): void; (): void; }) => {
        const { form } = props;
        if (value && value !== form.getFieldValue('password')) {
            callback('Hai Password không giống nhau!');
        } else {
            callback();
        }
    };

    const validateToNextPassword = (rule: any, value: any, callback: () => void) => {
        const { form } = props;
        if (value && confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    };

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 16,
                offset: 8,
            },
        },
    };

    return (
        <div className='login-card flex-center'>
            <div className='flex p-5 flex-col items-center'>
                <img src={require('./signup.jpg')} alt='login' />
                <a href="/auth/register">Create an account</a>
            </div>
            <Form {...formItemLayout} onSubmit={handleSubmit} className="form-info">
                <Form.Item label="E-mail" >
                    {getFieldDecorator('email', {
                        rules: [
                            {
                                type: 'email',
                                message: 'Định dạng không phải E-mail!',
                            },
                            {
                                required: true,
                                message: 'Hãy điền E-mail của bạn!',
                            },
                        ],
                    })(<Input />)}
                </Form.Item>
                <Form.Item label="Địa điểm">
                    {getFieldDecorator('address', {
                        rules: [{ required: true, message: 'Địa điểm hiện tại của ban!' }],
                    })(
                        <Selector defaultValue={'Địa điểm hiện tại của ban!'} data={user_city} />,
                    )}
                </Form.Item>
                <Form.Item label={<span>User Name</span>}>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Hãy điền username của bạn!', whitespace: true }],
                    })(<Input />)}
                </Form.Item>
                <Form.Item label="Password" hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [
                            {
                                required: true,
                                message: 'Hãy điền password của bạn!',
                            },
                            {
                                validator: validateToNextPassword,
                            },
                        ],
                    })(<Input.Password />)}
                </Form.Item>
                <Form.Item label="Confirm Password" hasFeedback>
                    {getFieldDecorator('confirm', {
                        rules: [
                            {
                                required: true,
                                message: 'Hãy xác nhận password!',
                            },
                            {
                                validator: compareToFirstPassword,
                            },
                        ],
                    })(<Input.Password onBlur={handleConfirmBlur} />)}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Register
                    </Button>
                </Form.Item>
            </Form>
        </div>

    );
};

const WrappedRegistrationForm: any = Form.create({ name: 'registration-form' })(RegistrationForm);

export default WrappedRegistrationForm;