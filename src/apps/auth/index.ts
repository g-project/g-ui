import genAsyncComponent from '@foundation/utils/router';
import { AuthReducer, AuthAction, SearchAction, SearchReducer } from './state';
import Module from '@foundation/registry/module';
import { combineReducers } from 'redux';

const appReducer = combineReducers({
    search: SearchReducer,
    auth: AuthReducer,
});

const rootReducer = (state: any, action: any) => {
    if (action.type === 'LOGOUT') {
        state = undefined;
    }
    return appReducer(state, action);
};

function setup(module: Module) {
    module.state({
        reducer: rootReducer,
        action: {AuthAction, SearchAction}
    });
    module.route('/login', {
        title: 'Timeline',
        secure: false,
        exact: true,
        component: genAsyncComponent({
            loader: () => import('./page/login')
        })
    });
    module.route('/setting', {
        title: 'Setting',
        secure: true,
        exact: true,
        component: genAsyncComponent({
            loader: () => import('./page/setting')
        })
    });
};

export { setup };
