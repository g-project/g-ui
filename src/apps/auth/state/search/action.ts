import { SEARCH_CITY } from './const';

function search_city(city: string) {
    return {
        type: SEARCH_CITY,
        payload: city
    };
}

function add_cityId(cityId: string) {
    return {
        type: SEARCH_CITY,
        payload: cityId
    };
}

const SearchAction = {
    search_city,
    add_cityId
};

export default SearchAction;