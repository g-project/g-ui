import { SearchStateType } from './types';

const SEARCH_CITY = 'SEARCH_CITY';
const ADD_CITYID = 'ADD_CITYID';

export {
    SEARCH_CITY,
    ADD_CITYID
};

const initState: SearchStateType =  {
    city: '',
    cityId: ''
};

export default initState;