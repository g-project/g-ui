import { SEARCH_CITY, ADD_CITYID } from './const';

type SearchCityAction  = {
    type: typeof SEARCH_CITY,
    payload: string;
}

type Add_CityId = {
    type: typeof ADD_CITYID,
    payload: string,
};

export type UserInfo = {
    email: string
}
export type CityInfo = {
    city: string;
    cityId: string
}

export type SearchActionType = SearchCityAction | Add_CityId;

export type SearchStateType = {
    city?: string,
    cityId?: string
}
