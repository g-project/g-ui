import initState, { SEARCH_CITY, ADD_CITYID } from './const';
import { SearchStateType, SearchActionType } from './types';

function reducer(state: SearchStateType = initState, action: SearchActionType): SearchStateType {
    switch (action.type) {
    case SEARCH_CITY: {
        return {
            ...state,
            city: action.payload
        };
    }
    case ADD_CITYID: {
        return {
            ...state,
            cityId: action.payload
        };
    }
    default: {
        return state;
    }
    }
}

export default reducer;