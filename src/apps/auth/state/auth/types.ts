import { LOGOUT, AUTHENTICATE, BOOTSTRAP, UPDATE_USER_INFO } from './const';

type LogoutAction = {
    type: typeof LOGOUT
}
type AuthenticatedAction = {
    type: typeof AUTHENTICATE
}
type BootstrapAction  = {
    type: typeof BOOTSTRAP
}
type UpdateUserInfoAction  = {
    type: typeof UPDATE_USER_INFO,
    payload: UserInfo
}

export type UserInfo = {
    email: string,
    full_name: string,
    user_id: string,
    user_city: string,
    recommend: Array<any>
}

export type AuthActionType = LogoutAction | AuthenticatedAction | BootstrapAction | UpdateUserInfoAction

export type AuthStateType = {
    isAuthenticated: boolean,
    token?: string,
    userInfo?: UserInfo
}