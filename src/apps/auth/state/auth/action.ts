import { BOOTSTRAP, LOGOUT, AUTHENTICATE, UPDATE_USER_INFO } from './const';
import { UserInfo } from './types';

function bootstrap() {
    return {
        type: BOOTSTRAP
    };
}

function authenticate() {
    return {
        type: AUTHENTICATE
    };
}

function logout() {
    return {
        type: LOGOUT
    };
}

function update_userinfo(info: UserInfo) {
    return {
        type: UPDATE_USER_INFO,
        payload: info
    };
}

const AuthAction = {
    authenticate,
    bootstrap,
    logout,
    update_userinfo,
};

export default AuthAction;
