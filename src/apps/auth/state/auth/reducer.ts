import initState, { LOGOUT, AUTHENTICATE, UPDATE_USER_INFO } from './const';
import { AuthStateType, AuthActionType } from './types';

function reducer(state: AuthStateType = initState, action: AuthActionType): AuthStateType {
    switch(action.type) {
    case AUTHENTICATE: {
        return {
            ...state,
            isAuthenticated: true
        };
    }
    case LOGOUT: {
        return {
            ...state,
            isAuthenticated: false
        };
    }
    case UPDATE_USER_INFO: {
        return {
            ...state,
            userInfo: action.payload
        };
    }
    default: {
        return state;
    }
    }
}

export default reducer;