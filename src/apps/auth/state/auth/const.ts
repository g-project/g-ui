import { AuthStateType } from './types';

const AUTHENTICATE = 'AUTHENTICATE';
const BOOTSTRAP = 'BOOTSTRAP';
const LOGOUT = 'LOGOUT';
const UPDATE_USER_INFO = 'UPDATE_USER_INFO';

export {
    AUTHENTICATE,
    BOOTSTRAP,
    LOGOUT,
    UPDATE_USER_INFO,
};

const initState: AuthStateType =  {
    isAuthenticated: false
};

export default initState;