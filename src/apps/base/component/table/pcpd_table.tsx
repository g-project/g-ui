import React from 'react';
import ReactTableBase from '@foundation/component/paginated_view/paginated_table';
import { Query, DataBundle } from '@foundation/component/paginated_view/services/pagination';
import { PCPDBundle } from 'apps/base/bundle';
import { PCPDTableProps, TableBaseStates } from '@foundation/component/paginated_view/type';
import { PCPDSimpleRowTable } from './row';
import { Icon, Empty } from 'antd';

import styles from './style.module.scss';

type PCPDTableStates<T> = TableBaseStates<T> & {
    showTool: boolean
}

export default class PCPDTable<T = {}> extends ReactTableBase<T, PCPDTableProps<T>, PCPDTableStates<T>> {
    constructor(props: any) {
        super(props);
        this.state = {
            ...this.state,
            showTool: false
        };
    }
    // Override
    get identifier(): string | null {
        return this.PS.metadata.identifier;
    }
    // Override
    get header(): Array<string> {
        const { headers } = this.props;
        if (headers) {
            return headers;
        }
        const fields = this.PS.metadata.fields;
        return Object.keys(fields)
            .filter(key => !fields[key].hidden)
            .map(key => key);
    }
    get title() {
        return this.PS.metadata.title;
    }
    get fields(): Record<string, any> {
        return this.PS.metadata.fields;
    }
    // Override
    renderHeader() {
        return (
            <div key={this.title} className={styles.header}>
                <span className={styles.title}>{this.title}</span>
                <input
                    // onChange={this.onFilter}
                    placeholder="Search..."
                    className={styles.search}
                    style={{ transition: 'width 0.2s' }}
                    type="text"
                />
            </div>
        );
    }
    // Override
    sort = (key: string) => {
        const { changeOrder } = this.props;
        if (this.fields[key].sortable) {
            this.setState((prev: any) => {
                changeOrder && changeOrder(key, prev.sortKey[key] === -1 ? 1 : -1);
                return {
                    sortKey: {
                        [key]: prev.sortKey[key] === -1 ? 1 : -1
                    },
                };
            });
        }
    }
    // Override
    renderRowContent(): any {
        const { data, activeItem, batchMode } = this.state;
        const { Item } = this.props;
        const ItemComponent = Item || PCPDSimpleRowTable;
        const row_header = this.header;
        if (data.length === 0) {
            return (
                <tr className={styles.rowContent}>
                    <td colSpan={Object.keys(this.header).length}>
                        <div>
                            {<Empty />}
                        </div>
                    </td>
                </tr>
            );
        }
        const rows = data.map((item: T, index: number) => {
            const active = this.extractKey(activeItem) === this.extractKey(item);
            const selected = this.extractKey(item) in this.state.selectedItem;
            return (<ItemComponent
                active={active}
                selected={selected}
                data={item}
                fields={this.fields}
                headers={row_header}
                index={index}
                key={`${this.extractKey(item)}-${index}`}
                setActiveItem={this.setActiveItem}
                batchMode={batchMode}
                selectItem={this.selectItem}
                contentLayout={this.contentLayout}
            />);
        });
        return rows;
    }
    // Override
    _renderRowHeader() {
        const { sortKey, batchMode } = this.state;
        return (
            <>
                <tr>
                    {batchMode && (
                        <td className="px-md py-sm bg-second font-bold">
                            <div className="flex justify-center">
                                <Icon type="setting"/>
                            </div>
                        </td>
                    )}
                    {this.header.map((key: string) => {
                        const label = this.fields[key].label;
                        return (<td className="p-md bg-second font-bold" onClick={() => this.sort(key)} key={key}>
                            <span className="text-on-second">{label}</span>
                            <Icon
                                className={sortKey[key] ? 'text-red-700' : 'text-transparent'}
                                type={sortKey[key] === 1 ? 'sort-ascending' : 'sort-descending'}
                            />
                        </td>);
                    })}
                </tr>
            </>
        );
    }
    _renderHeaderCell(rowLayout: any, start: number) {
        const { sortKey } = this.state;
        return rowLayout.map((rowSpan: number, index: number) => {
            const head_index = start + index;
            const key = this.header[head_index];

            const label = this.fields[key] && this.fields[key].label;

            return (
                <td rowSpan={rowSpan} className={styles.headerCell} onClick={() => this.sort(key)} key={key}>
                    {label}
                    <Icon
                        className={sortKey[key] ? 'text-red-700' : 'text-transparent'}
                        type={sortKey[key] === 1 ? 'sort-ascending' : 'sort-descending'}
                    />
                </td>
            );
        });
    }
    renderRowHeader() {
        let start = 0;
        const contentLayout = this.contentLayout;
        return contentLayout.map((rowLayout: number[], index: number) => {
            const maxCol = contentLayout[index-1] ? contentLayout[index-1].length : 0;
            start += maxCol;
            return (
                <tr key={index} className={styles.rowHeader}>
                    {index === 0 && this.props.batchMode && (
                        <td rowSpan={this.contentLayout.length}>
                            <div>
                                <Icon type="setting"/>
                            </div>
                        </td>
                    )}
                    {this._renderHeaderCell(rowLayout, start)}
                </tr>
            );
        });
    }
    // Override
    fetch(query: Query): Promise<DataBundle<T>> {
        // This function define how to get data
        return PCPDBundle(this.props.source, query);
    }
}
