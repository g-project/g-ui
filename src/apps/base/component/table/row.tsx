import React from 'react';
import { WidgetRegistry } from '@foundation/registry';
import { PCPDItemTable } from '@foundation/component/paginated_view/type';
import { Checkbox, Icon } from 'antd';

export const PCPDSimpleRowTable = React.memo(({ data, headers, setActiveItem, active, fields, batchMode, selectItem, selected, contentLayout }: PCPDItemTable): any => {
    function _renderCell(data: any, item: any) {
        const Widget = WidgetRegistry.get('global', item.datatype);
        if (Widget) {
            return <Widget>{data}</Widget>;
        }
        return <>{data}</>;
    }
    function _renderRow(rowLayout: any, start: number) {
        return rowLayout.map((colSpan: number, index: number) => {
            const head_index = start + index;
            const key = headers[head_index];
            const item: any = fields[key];

            try {
                const [dtype, embed_key] = item['datatype'].split(':');
                let value = data[item.key];
                if (dtype === 'embed') {
                    value = data[embed_key];
                }
                return (
                    <td
                        rowSpan={colSpan}
                        onClick={() => setActiveItem(data)}
                        key={`${key}-${index}`}
                        className={`${active && 'text-white'} p-xs p-0 group-hover:text-white hover:bg-blue-300`}
                    >
                        {_renderCell(value, item)}
                    </td>
                );
            } catch(e) {
                return null;
            }
        });
    }
    function _renderTool() {
        return (
            <td rowSpan={contentLayout.length} className="px-sm py-sm bg-second font-bold group-hover:text-blue-500 group-hover:bg-blue-200">
                <div className="flex justify-center items-center">
                    <Checkbox checked={selected} onChange={() => selectItem(data)} />
                    <Icon className="ml-sm" type="plus" />
                </div>
            </td>
        );
    }
    let start = 0;
    return contentLayout.map((rowLayout: number[], index: number) => {
        const maxCol = contentLayout[index-1] ? contentLayout[index-1].length : 0;
        start += maxCol;
        return (
            <tr
                key={index}
                className={`${index === 0 ? 'border-t-2' : 'border-t'} group select-none hover:bg-blue-500 bg-white ${active ? 'bg-blue-600' : ''}`}
            >
                {index === 0 && batchMode && _renderTool()}
                {_renderRow(rowLayout, start)}
            </tr>
        );
    });
});