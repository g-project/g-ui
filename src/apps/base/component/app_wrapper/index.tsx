import React from 'react';
import NavBar from '../nav_bar';
import './style.scss';
import Footer from '../footer';


const AppWrapper = (props: any) => {
    console.log('Props', props);
    return (
        <div style={{ backgroundColor: 'rgba(0, 0, 0, 0)', minHeight: '100vh', display: 'flex', flexDirection: 'column' }}>
            <NavBar />
            <div className="flex flex-col flex-1 flex-grow">
                {props.children}
            </div>
            <Footer siteName={'SITE_NAME'} />
        </div>
    );
};

export default AppWrapper;
