import { flatten } from './utils';

const NAV = [
    {
        title: 'Subscriber',
        key: 'subscriber',
        link: '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}',
        child: [
            {
                title: 'Subscriber Drilldown',
                key: 'drilldown',
                link: '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}',
                child: [
                    {
                        title: 'Subscriber Drilldown',
                        key: 'one',
                        link: '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}',
                    },
                    {
                        title: 'Health Plan',
                        key: 'two',
                        link: '/dashboard/healthplan/claim/filter?qe={"filter:meta":"subscriber:meta"}',
                    }
                ]
            },
            {
                title: 'Health Plan',
                key: 'health_plan',
                link: '/dashboard/healthplan/claim/filter?qe={"filter:meta":"subscriber:meta"}',
            }
        ]
    },
    {
        title: 'Billing Provider',
        link: '/dashboard/billing-provider/claim/filter?qe={"filter:meta":"billing-provider:meta"}',
        key: 'billing',
    },
    {
        title: 'Rendering Provider',
        link: '/dashboard/rendering-provider/claim/filter?qe={"filter:meta":"rendering-provider:meta"}',
        key: 'rendering'
    }
];

const FLATTEN_RESULT = {
    'subscriber.drilldown.one':
    {
        title: 'Subscriber Drilldown',
        key: 'subscriber.drilldown.one',
        link:
            '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}'
    },
    'subscriber.drilldown.two':
    {
        title: 'Health Plan',
        key: 'subscriber.drilldown.two',
        link:
            '/dashboard/healthplan/claim/filter?qe={"filter:meta":"subscriber:meta"}'
    },
    'subscriber.drilldown':
    {
        title: 'Subscriber Drilldown',
        key: 'subscriber.drilldown',
        link:
            '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}'
    },
    'subscriber.health_plan':
    {
        title: 'Health Plan',
        key: 'subscriber.health_plan',
        link:
            '/dashboard/healthplan/claim/filter?qe={"filter:meta":"subscriber:meta"}'
    },
    subscriber:
    {
        title: 'Subscriber',
        key: 'subscriber',
        link:
            '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}'
    },
    billing:
    {
        title: 'Billing Provider',
        link:
            '/dashboard/billing-provider/claim/filter?qe={"filter:meta":"billing-provider:meta"}',
        key: 'billing'
    },
    rendering:
    {
        title: 'Rendering Provider',
        link:
            '/dashboard/rendering-provider/claim/filter?qe={"filter:meta":"rendering-provider:meta"}',
        key: 'rendering'
    }
};

describe('Nav item flatten', () => {
    it('Transform nav object without error', () => {
        const spec = flatten(NAV);
        expect(spec).toEqual(FLATTEN_RESULT);
    });
});
