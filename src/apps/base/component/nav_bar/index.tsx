import React, { useState } from 'react';
import { SITE_NAME, APP_NAME } from 'config';
import Icon from 'antd/lib/icon';
import { Divider, Avatar } from 'antd';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import SimpleDropdown from '@foundation/component/dropdown';
import style from './style.module.scss';
import { LOGOUT } from 'apps/auth/state/auth/const';
import { useHistory, useLocation, Route } from 'react-router';


function NameInitials(name: string) {
    return name && name.split(/\s+/).map((s: string) => s[0]).join('');
}

function MenuOverlay(props: any) {
    const userInfo = props.userInfo;
    const history = useHistory();
    const dispatch = useDispatch();
    const logout = () => {
        console.log('props =>>>>>>>>', props);
        dispatch({ type: LOGOUT });
        history.push('/auth');
    };

    return (
        <div className="mx-auto flex flex-col bg-white rounded shadow overflow-hidden">
            <div className="flex p-2 items-center hover:bg-blue-100">
                <Avatar size="small" style={{ marginRight: 10 }}>U</Avatar>
                <span>{userInfo && userInfo.email}</span>
            </div>
            <Divider dashed style={{ margin: '0px 0px' }} />
            <a href='/user/user_info' className="flex p-3 items-center hover:bg-blue-100">
                <Icon className="mr-2" type="user" />
                Cá nhân
            </a>
            <Link to="/setting" className="flex p-3 items-center hover:bg-blue-100">
                <Icon className="mr-2" type="setting" />
                Cài đặt
            </Link>
            <Divider dashed style={{ margin: '0px 0px' }} />
            <div className="flex p-3 items-center hover:bg-blue-100" onClick={logout}>
                <Icon className="mr-2" type="logout" /> Đăng xuất
            </div>
        </div>
    );
};


export default function NavBar(): JSX.Element {
    const location = useLocation();
    console.log('location', location);
    const [isShow, showMenu] = useState(false);
    const userInfo = useSelector((state: any) => state.auth.auth.userInfo);
    const isAuthenticated = useSelector((state: any) => state.auth.auth.isAuthenticated);

    return (
        <div className={`${isShow && style.navOpen} ${style.navBar}`}>
            <Route path={['/user', '/search', '/location', '/auth']} render={() => {
                return (
                    <div className={style.branchName__right}>
                        <div className="flex items-center">
                            <Avatar
                                size="large"
                                className="mr-md"
                                src={require('apps/blank/page/home/homeNotAuth/logo.png')}
                            />
                            <Link to="/">
                                <div className="flex flex-col items-start sm:items-start w-auto">
                                    <span className="text-lg text-on-primary">{SITE_NAME}</span>
                                    <span className="text-xs text-on-primary">{APP_NAME}</span>
                                </div>
                            </Link>
                        </div>
                        <Icon onClick={() => showMenu(state => !state)} type="menu" className={`${style.menuToggle}`} />
                    </div>
                );
            }} />
            <Route path="/" exact render={() => {
                return (
                    <div className={style.branchName}>
                        <div className="flex items-center">
                            <Avatar
                                size="large"
                                className="mr-md"
                                src={require('apps/blank/page/home/homeNotAuth/logo.png')}
                            />
                            <Link to="/">
                                <div className="flex flex-col items-start sm:items-start w-auto">
                                    <span className="text-lg text-on-primary">{SITE_NAME}</span>
                                    <span className="text-xs text-on-primary">{APP_NAME}</span>
                                </div>
                            </Link>
                        </div>
                        <Icon onClick={() => showMenu(state => !state)} type="menu" className={`${style.menuToggle}`} />
                    </div>
                );
            }} />

            <nav className={`${style.navItemGroup} ${style.navItemRight}`}>
                {isAuthenticated ?
                    (<SimpleDropdown
                        overlay={<MenuOverlay userInfo={userInfo} />}
                        className="right-0 z-50 pt-xs"
                    >
                        <div className="flex flex-grow items-center cursor-pointer hover:bg-second px-sm">
                            <Avatar className="select-none">{NameInitials(userInfo.full_name)}</Avatar>
                            <span className="select-none">&nbsp;{userInfo.full_name || 'Avatar'}</span>
                        </div>
                    </SimpleDropdown>)
                    :
                    (
                        <Link className={style.navItem} to={'/auth'}>Login</Link>
                    )
                }
            </nav>
        </div>
    );
}
