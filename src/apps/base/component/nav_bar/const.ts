import { flatten } from './utils';

const NAV = [
    {
        title: 'Subscriber',
        key: 'subscriber',
        link: '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}',
        child: [
            {
                title: 'Subscriber Drilldown',
                key: 'drilldown',
                link: '/dashboard/subscriber/claim/filter?qe={"filter:meta":"subscriber:meta"}',
            },
            {
                title: 'Health Plan',
                key: 'health_plan',
                link: '/dashboard/healthplan/claim/filter?qe={"filter:meta":"subscriber:meta"}',
            }
        ]
    },
    {
        title: 'Billing Provider',
        link: '/dashboard/billing-provider/claim/filter?qe={"filter:meta":"billing-provider:meta"}',
        key: 'billing',
    },
    { 
        title: 'Rendering Provider', 
        link: '/dashboard/rendering-provider/claim/filter?qe={"filter:meta":"rendering-provider:meta"}', 
        key: 'rendering' 
    }
];

export { NAV, flatten };
