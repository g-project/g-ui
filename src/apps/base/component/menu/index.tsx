import React, { useState } from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
import SimpleDropdown from '@foundation/component/dropdown';

type MenuItem = {
    title: string,
    key: string,
    link: string,
    child: Array<MenuItem>
}

type MenuItemProps = {
    data: MenuItem,
    active?: boolean,
    selectMenuItem: (item: MenuItem) => void,
}
type SubMenuItemProps = MenuItemProps & {
    root: MenuItem,
    data: Array<MenuItem>,
    selectedMenu: MenuItem,
}

type MenuGroupProps = {
    data?: any
    selectedMenu: MenuItem
    selectMenuItem: (item: MenuItem) => void,
}

function MenuItem({ data, active, selectMenuItem }: MenuItemProps) {
    return (
        <Link
            onClick={() => selectMenuItem(data)}
            className={`select-none hover:bg-select hover:text-white p-md whitespace-no-wrap ${active ? 'bg-select' : ''}`}
            to={data.link}
        >
            {data.title}
        </Link>
    );
};

function SubMenu({ data, root, selectMenuItem, selectedMenu }: SubMenuItemProps) {
    const [show, setShow] = useState(false);
    return (
        <div className="select-none flex flex-col w-48">
            <div
                onClick={() => setShow(state => !state)}
                className="flex justify-between items-center p-md hover:bg-select hover:text-white"
            >
                <span className="whitespace-no-wrap">{root.title}</span>
                <Icon type={show ? 'minus' : 'plus'} />
            </div>
            <div className="flex flex-col bg-second">
                {show && data.map((item: any) => {
                    if (item.child) {
                        return <SubMenu root={item} selectedMenu={selectedMenu} data={item.child} selectMenuItem={selectMenuItem}/>;
                    }
                    return <MenuItem
                        key={item.title} 
                        selectMenuItem={selectMenuItem} 
                        data={item} 
                        active={selectedMenu.key === item.key}
                    />;
                })}
            </div>
        </div>
    );
};

function MenuWrap({ data, selectMenuItem, selectedMenu }: MenuGroupProps) {
    return (
        <div className="flex border bg-primary border-primary items-start shadow">
            {data.map((item: any) => {
                if (item.child) {
                    return <SubMenu 
                        key={item.title} 
                        root={item} 
                        data={item.child} 
                        selectMenuItem={selectMenuItem}
                        selectedMenu={selectedMenu}
                    />;
                }
                return <MenuItem 
                    key={item.title} 
                    selectMenuItem={selectMenuItem} 
                    data={item} 
                    active={selectedMenu.key === item.key}
                />;
            })}
        </div>
    );
};

function Menu({ data, menuFlatten }: any) {
    function getDefaultLocation(): MenuItem {
        const path_name = window.location.pathname;
        const menu_key = Object.keys(menuFlatten).find((key: string) => {
            const item: MenuItem = menuFlatten[key];
            return item.link.startsWith(path_name);
        });
        if (menu_key) {
            return menuFlatten[menu_key];
        }
        return {} as MenuItem;
    }
    const itemMenu: MenuItem = getDefaultLocation();
    const [selectedMenu, setSelectedKey] = useState<MenuItem>(itemMenu);
    function selectMenuItem(item: any) {
        setSelectedKey(item);
    }
    return (
        <SimpleDropdown
            overlay={<MenuWrap selectedMenu={selectedMenu} selectMenuItem={selectMenuItem} data={data} />}
        >
            <div className="flex flex-1 items-center p-sm justify-center hover:bg-second">
                <Icon type="menu" style={{ fontSize: 20 }} />
                <span className="ml-sm font-medium text-base">{selectedMenu.title}</span>
            </div>
        </SimpleDropdown>
    );
}

export default Menu;
export { SubMenu };
