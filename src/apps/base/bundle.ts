import Immutable from 'immutable';
import { DataProfile } from './access';

const  PCPDBundle = async (url: string, query: any) => {
    let _pagination = query['pagination'];
    let _query = [];
    let _sort = [];
    let _metadata = {filters: [], orders: [], fields: [], title: '', identifier: null};

    if (query['filter'] && Immutable.isList(query['filter'])) {
        _query = query['filter'].toJS().filter((item: any) => {
            const op = Object.keys(item)[0].split(':')[1];
            return op !== 'sort';
        });
        _sort = query['filter'].toJS().filter((item: any) => {
            const op = Object.keys(item)[0].split(':')[1];
            return op === 'sort';
        }).map((item: any) => {
            const [key, value]: any = Object.entries(item)[0];
            return [key.split(':')[0], value].join('.');
        });
    }

    const data = await DataProfile.Get(url, {
        params: {
            max_results: _pagination.max_results,
            page: _pagination.page,
            where: JSON.stringify(_query),
            sort: JSON.stringify(_sort)
        }
    });
    const meta_resp = await DataProfile.Get(`${url}:meta`);

    if (meta_resp.status === 200) {
        _metadata = meta_resp.data;
    }
    const range = data.headers['content-range'];

    try {
        const [, total]: [any, number] = range.split('/');
        _pagination = {
            page: _pagination.page,
            max_results: _pagination.max_results,
            total: Number(total)
        };
    } catch(e) {
        _pagination = {
            page: 1,
            max_results: 25,
            total: 0
        };
    }

    const bundle = {
        '_items': data.data,
        '_metadata': _metadata,
        '_pagination': _pagination
    };
    return bundle;
};

const  EveBundle = async (url: string, query: any) => {
    const meta = query['meta'];
    const _query = query['filter'];
    const data = await DataProfile.Get(url, {
        params: {
            max_results: meta.max_results,
            page: meta.page,
            where: JSON.stringify(_query),
        }
    });
    return {
        '_items': data.data['_items'],
        '_pagination': data.data['_meta']
    };
};

export { PCPDBundle, EveBundle };
