// Setup datalayer
import { API_URL, AUTH_URL, REPORT_URL } from 'config';
import DataAccess from '@foundation/access';

export const DataProfile = new DataAccess('data', API_URL);
export const OAuthProfile = new DataAccess('auth', AUTH_URL);
export const ReportProfile = new DataAccess('report', REPORT_URL);
