import { useState, useCallback, useEffect } from 'react';
import { DataProfile } from 'apps/base/access';

function useGetData<T = {}>(query: string): [boolean, T | null, any, any] {
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState<T | null>(null);
    const [err, setErr] = useState(null);

    const fetchData = useCallback(async () => {
        await setLoading(true);
        try {
            const dataGet = await DataProfile.Get(query);
            if (dataGet.data._items) {
                setData(dataGet.data._items);
            }
            else setData(dataGet.data);
        } catch (e) {
            setErr(e);
        } finally {
            await setLoading(false);
        }
    }, [query]);

    const reloadData = () => fetchData();

    useEffect(() => {
        query && fetchData();
    }, [fetchData, query]);
    return [loading, data, err, reloadData];
}

export default useGetData;