import Module from '@foundation/registry/module';
import { Login, Home, UserPage, Location } from './page';
import HomePublic from './page/home/homeNotAuth';

function setup(module: Module) {
    module.route('/', {
        title: 'Home',
        exact: true,
        secure: false,
        component: Home
    });
    module.route('/auth', {
        title: 'Login',
        exact: false,
        secure: true,
        component: Login
    });
    module.route('/user', {
        title: 'User',
        exact: false,
        secure: true,
        component: UserPage
    });
    module.route('/location', {
        title: 'location',
        exact: false,
        secure: true,
        component: Location
    });
    module.route('/search', {
        title: 'location',
        exact: false,
        secure: true,
        component: HomePublic
    });
}

export { setup };
