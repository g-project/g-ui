import React from 'react';

import Form from 'react-jsonschema-form';
import { JSONSchema6 } from 'json-schema';

import { schema } from './schema.json';

type ContentFormProps = {
    className?: string
}

const ContentForm = ({className}: ContentFormProps) => {
    return (
        <Form
            className={`bootstrap ${className}`}
            schema={schema as JSONSchema6}
        />
    );
};

export default ContentForm;