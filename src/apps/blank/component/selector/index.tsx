import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

const Selector = (props: any) => {
    return (
        <Select defaultValue={props.defaultValue} style={{ 'width': '100%' }} onChange={props.onChange}>
            {
                props.data.map((item: any) => {
                    return (
                        <Option value={item.id}>{item.title}</Option>
                    );
                })
            }
        </Select>
    );
};

export default Selector;