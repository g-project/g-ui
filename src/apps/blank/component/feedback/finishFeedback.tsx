import React from 'react';
import './style.scss';
import { LocationTitleFinish, UsernameTitleFinish } from './title';

export const FinishFeedback = (props: any) => {
    console.log('props FinishFeedback', props);
    return (
        props.user ?
            <UsernameTitleFinish parent={props.parent} dataPartner={props.dataPartner} dataSeason={props.dataSeason} />
            :
            <LocationTitleFinish parent={props.parent} dataPartner={props.dataPartner} dataSeason={props.dataSeason} />
    );
};