import React from 'react';
import './style.scss';
import Selector from '../selector';
import { partner, season } from 'apps/data';

export const SelectorContext = (props: any) => {
    function PartnerChange(value: string) {
        return props.PartnerChange(value);
    }
    function SeasonChange(value: string) {
        return props.SeasonChange(value);
    }

    return (
        <div className='flex justify-between p-2'>
            <div className='flex items-center mr-5'>
                Bạn muốn đi cùng:
                <div className="seletor">
                    <Selector defaultValue={props.data[0]} onChange={PartnerChange} data={partner} />
                </div>
            </div>
            <div className='flex items-center'>
                Vào thời gian:
                <div className="seletor">
                    <Selector defaultValue={props.data[1]} onChange={SeasonChange} data={season} />
                </div>
            </div>
        </div>
    );
};