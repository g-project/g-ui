import React, { useState } from 'react';
import './style.scss';
import { Button, Select, notification } from 'antd';
import { DataProfile } from 'apps/base/access';
import { useSelector } from 'react-redux';
import useGetData from 'apps/hook/useGetData';
const { Option } = Select;

const ListInvitation = (props: any) => {
    return (
        <div className="list-invitation">
            <div>
                Danh sách User đi cùng:
            </div>
            <div>
                {props.nameUser.map((item: any, i: number) => {
                    return (
                        <div>
                            {item.username}: {item.contact}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

const ShowOption = (props: any) => {
    const stateInvite = props.state;
    const [nameUser, setNameUser] = useState(props.user_invite);
    const [nameUser2, setNameUser2] = useState(props.user_invite);
    const [state, setState] = useState(false);
    const [state2, setState2] = useState(false);
    const [invited_user, setInvitedUser] = useState<any[]>([]);

    async function get_user_invite() {
        const data = await DataProfile.Post('/invite_user', { data: { 'location_id': props.location, 'user_id': props.user } });
        const array = await Promise.all(data.data.user.map(async (item: any) => {
            const dataUser = await DataProfile.Get(`/user/${item[1]}`);
            const col = {};
            col['_id'] = item[0];
            col['username'] = dataUser.data.username;
            col['user_id'] = item[1];
            col['_etag'] = item[3];
            col['contact'] = dataUser.data.email;
            return col;
        }));
        setNameUser2(array as any);
    }

    async function get_user_invite_new() {
        const array = await Promise.all(nameUser.map(async (item: any) => {
            console.log(item);
            let dataUser: any = {};
            if (item.invitee === props.user) {
                dataUser = await DataProfile.Get(`/user/${item.invitor}`);
            } else {
                dataUser = await DataProfile.Get(`/user/${item.invitee}`);
            }
            const col = {};
            col['username'] = dataUser.data.username;
            col['contact'] = dataUser.data.email;
            return col;
        }));
        setNameUser(array as any);
    }

    if (!state && stateInvite) {
        get_user_invite_new();
        setState(true);
    }

    function handleChange(value: any) {
        console.log(`selected ${value}`);
        setInvitedUser(value);
    }

    function send_invitation() {
        try {
            invited_user.map(async (item: any) => {
                const userID = nameUser2.find((ind: any) => {
                    return ind._etag === item;
                });
                console.log('invited user', userID);
                await DataProfile.Post('/invitation', { data: { 'invitor': props.user, 'invitee': userID.user_id, 'location_id': props.location, 'status': 'pending' } });
            });
        } catch (e) {
            console.log(e);
            notification.open({
                message: 'Thất bại',
                description:
                    'Không thể mời các user đó!!!',
                onClick: () => {
                    console.log('Notification Clicked!');
                },
            });
        } finally {
            notification.open({
                message: 'Thành Công',
                description:
                    'Đã Mời các user khác tham gia thành công. Hãy chờ họ xác nhận tham gia.',
                onClick: () => {
                    console.log('Notification Clicked!');
                },
            });
            setState2(false);
        }
 
    }

    return (
        <div>
            <Button className="comment-per-user-Avatar" onClick={() => { setState2(!state2); get_user_invite(); }}>
                Mời những users khác?</Button>
            {state2 ?
                <>
                    <Select
                        mode="multiple"
                        style={{ width: '100%' }}
                        placeholder="Please select"
                        onChange={handleChange}
                    >
                        {nameUser2.map((i: any) => {
                            return (
                                <Option key={i._etag}>{i.username}</Option>
                            );
                        })}
                    </Select>
                    <Button onClick={send_invitation}>
                        Gữi Lời Mời
                    </Button>
                </> : null}
            {
                nameUser.length > 0 ?
                    <ListInvitation nameUser={nameUser} />
                    : null
            }
        </div>
    );
};

export const MultiSelector = (props: any) => {
    const user = useSelector((state: any) => state.auth.auth.userInfo.user_id);
    const [, user_invite,] = useGetData<[]>(`/invitation?where={"$or": [{"invitor":"${user}"}, {"invitee":"${user}"}],"status": "accepted","location_id": "${props.location}"}`);
    if (!user_invite) return null;
    return (
        <ShowOption reload={props.reload} user={user} user_invite={user_invite} location={props.location} state={user_invite.length > 0 ? true : false} />
    );
};