import React, { useState, ChangeEvent } from 'react';
import './style.scss';
import { Input, Button, Rate, notification } from 'antd';
import { DataProfile } from 'apps/base/access';
import { FinishFeedback } from './finishFeedback';
import { SelectorContext } from './selectorContext';
import { MultiSelector } from './multiSelector';
import { season, partner } from 'apps/data';
import { UsernameTitle, LocationTitle } from './title';
import { useHistory } from 'react-router';
import { randomStr } from 'util/function';

type FeedbackShowProps = {
    parent: Object;
    finish?: boolean;
    user?: boolean
    deleteItem?: any
    updateItem?: any
    postItem?: any
}
var Ajv = require('ajv');
const FeedbackShow = React.memo(function FeedbackShow(props: FeedbackShowProps) {
    const history = useHistory();
    console.log('history', history);

    var ajv = new Ajv({ $data: true });
    var schema = {
        'properties': {
            'location_id': { 'type': 'string' },
            'user_id': { 'type': 'string' },
            'location_city': { 'type': 'string' },
            'user_city': { 'type': 'string' },
            'comment': { 'type': 'string', 'minLength': 4 },
            'score': { 'type': 'number' },
            'season_id': { 'type': 'string' },
            'partner_id': { 'type': 'string' },
        }
    };

    const parent = props.parent[0];
    const [partner_id, setPartnerId] = useState(parent.partner_id || 'Đi Cùng Ai');
    const [season_id, setSeasonId] = useState(parent.season_id || 'Đi Vào Lúc Nào');
    const [comment, setComment] = useState(parent.comment);
    const [score, setScore] = useState(parent.score);

    const PartnerChange = (value: string) => {
        setPartnerId(value);
    };
    const SeasonChange = (value: string) => {
        setSeasonId(value);
    };
    const CommentChange = (e: ChangeEvent<HTMLInputElement>) => {
        setComment(e.currentTarget.value);
    };
    function ScoreChange(value: number) {
        setScore(value);
    }
    function postItem() {
        return props.postItem();
    }

    const command_finish = 'feedback';
    const postdata = { location_id: parent.location_id, user_id: parent.user_id, location_city: parent.location_city, user_city: parent.user_city, partner_id: partner_id, season_id: season_id, comment: comment, score: score };
    var val = ajv.validate(schema, postdata);
    const post_Finish = async () => {
        if (val) {
            try {
                await DataProfile.Post(command_finish, { data: postdata });
            } catch (e) {
                console.log(e);
            } finally {
                notification.open({
                    message: 'Thành Công',
                    description:
                        'Đã Cập nhật chuyến đi thành công',
                    onClick: () => {
                        console.log('Notification Clicked!');
                    },
                });
            }
        } else {
            notification.open({
                message: 'Thất bại',
                description:
                    'Cập nhật chuyến đi thất bại',
                onClick: () => {
                    console.log('Notification Clicked!');
                },
            });
        }
    };

    async function delInvitation() {
        try {
            const dataNew = await DataProfile.Get(`invitation?where={"$or": [{"invitee": "${parent.user_id}"}, {"invitor": "${parent.user_id}"}]}`);
            (dataNew as any).data._items.map(async (item: any) => {
                const headersInv = {
                    'Content-Type': 'application/json',
                    'If-Match': item._etag
                };
                const invitation = `invitation/${item._id}`;
                console.log('test1');
                await DataProfile.Delete(invitation, { headers: headersInv });
            });
            console.log('test2', (dataNew as any));
        } catch (e) {
            console.log('Loi', e);
        } finally {
            DeleteData(parent._id);
        }
    }

    const headers = {
        'Content-Type': 'application/json',
        'If-Match': parent._etag
    };

    const UpdateData = async (id: string) => {
        const post_saving = `feedback_saving/${id}`;
        return props.updateItem(post_saving, headers, postdata);
    };

    const DeleteData = async (id: string) => {
        const post_saving = `feedback_saving/${id}`;
        return props.deleteItem(post_saving, headers);
    };

    const dataSeason = season.find((item: any) => {
        return item.id === parent.season_id;
    });
    const dataPartner = partner.find((item: any) => {
        return item.id === parent.partner_id;
    });

    if (props.finish) {
        console.log('props', props);
        return (
            <FinishFeedback user={props.user} parent={parent} dataPartner={dataPartner} dataSeason={dataSeason} />
        );
    }


    return (
        <div className='comment-per-user border'>
            <div className='comment-per-user-Info'>
                <div className="flex flex-col">
                    {props.user ? <UsernameTitle user_id={parent.user_id} /> : <LocationTitle location_id={parent.location_id} />}
                    <SelectorContext
                        PartnerChange={(item: any) => PartnerChange(item)}
                        SeasonChange={(item: any) => SeasonChange(item)}
                        data={[partner_id, season_id]}
                    />
                    <MultiSelector location={parent.location_id} reload={postItem} />
                    <div className='flex items-center p-2'>
                        <h1 className='comment-per-user-label'>Comment:</h1><Input onChange={CommentChange} placeholder={parent.comment} value={comment} />
                    </div>
                    <div className='flex items-center p-2'>
                        <h1 className='comment-per-user-label'>Score:</h1><Rate allowHalf defaultValue={score} onChange={ScoreChange} />
                    </div>
                </div>
                <div className='comment-per-user-btn'>
                    <Button className='w-full cmt-btn cmt-btn__sav' onClick={() => { UpdateData(parent._id); postItem(); }}>Lưu Tạm</Button>
                    <Button className='w-full cmt-btn' onClick={() => { post_Finish(); delInvitation(); DeleteData(parent._id); postItem();}}>Đã Đi</Button>
                    <Button className='w-full cmt-btn cmt-btn__del' onClick={() => { delInvitation(); postItem();}}>Xoá</Button>
                </div>
            </div>
        </div>
    );
});

export default FeedbackShow;