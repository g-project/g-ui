import React from 'react';
import useGetData from 'apps/hook/useGetData';
import { Avatar } from 'antd';
import StarRatingComponent from 'react-star-rating-component';

export const UsernameTitleFinish = (parent: any) => {
    let query_userId = `user?where={"_id":"${parent.parent.user_id}"}`;
    const [, dataUser,] = useGetData<{}>(query_userId);
    if (!dataUser) return null;
    return (
        <div className='comment-per-user comment-per-user__finish'>
            <div className='comment-per-user-Avatar'>
                <Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>{dataUser[0].username}</Avatar>
            </div>
            <div className='comment-per-user-Info'>
                <div className="flex items-center justify-between">
                    <div className='title-label'>
                        {dataUser[0].username}
                    </div>
                    <div className='comment-per-user-Score'>
                        <StarRatingComponent
                            name="rate1"
                            starCount={5}
                            value={parent.parent.score}
                        />
                    </div>
                </div>
                <div className='italic'>
                    {parent.dataPartner.title}: {parent.dataSeason.title}
                </div>
                <div>
                    {parent.parent.comment}
                </div>
            </div>
        </div>
    );
};
export const LocationTitleFinish = (parent: any) => {
    let query_locationId = `location?where={"_id":"${parent.parent.location_id}"}`;
    const [, dataLocation,] = useGetData<{}>(query_locationId);
    if (!dataLocation) return null;
    console.log('dataLocation', dataLocation);
    return (
        <div className='comment-per-user comment-per-user__finish'>
            <div className='comment-per-user-Info'>
                <div className="flex items-center justify-between">
                    <div className='title-label'>
                        {dataLocation[0].title}
                    </div>
                    <div className='comment-per-user-Score'>
                        <StarRatingComponent
                            name="rate1"
                            starCount={5}
                            value={parent.parent.score}
                        />
                    </div>
                </div>
                <div className='italic'>
                    {parent.dataPartner.title}: {parent.dataSeason.title}
                </div>
                <div>
                    {parent.parent.comment}
                </div>
            </div>
        </div>
    );
};


export const UsernameTitle = (parent: any) => {
    let query_userId = `user?where={"_id":"${parent.user_id}"}`;
    const [, dataUser,] = useGetData<{}>(query_userId);
    if (!dataUser) return null;
    return (
        <div className="flex items-center justify-center">
            <div className='title-label title-label__text-xl'>
                {dataUser[0].username}
            </div>
        </div>
    );
};
export const LocationTitle = (parent: any) => {
    let query_locationId = `location?where={"_id":"${parent.location_id}"}`;
    const [, dataLocation,] = useGetData<{}>(query_locationId);
    if (!dataLocation) return null;
    console.log('dataLocation', dataLocation);
    return (
        <div className="flex items-center justify-center">
            <div className='title-label title-label__text-xl'>
                {dataLocation[0].title}
            </div>
        </div>
    );
};