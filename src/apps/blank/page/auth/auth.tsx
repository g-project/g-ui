import React from 'react';
import { Tabs, Icon } from 'antd';
import WrappedNormalLoginForm from 'apps/auth/page/login';
import RegistrationForm from 'apps/auth/page/register';
import { Route, useHistory } from 'react-router';
import './styles.scss';

const { TabPane } = Tabs;


export default function Auth_Component() {
    const history = useHistory();
    return (
        <div className="flex justify-center items-start flex-1 border">
            <div className="flex justify-center min-w-sm">
                <Tabs defaultActiveKey={history.location.pathname==='/auth'?'1':'2'} className="w-full">
                    <TabPane
                        tab={
                            <div onClick={() => {
                                history.push('/auth');
                            }}>
                                <Icon type="login" />
                                Đăng nhập
                            </div>
                        }
                        key="1"
                    >
                        <Route
                            path="/auth"
                            render={(props) => {
                                return <WrappedNormalLoginForm props={props} />;
                            }}
                        />
                    </TabPane>
                    <TabPane
                        tab={
                            <div onClick={() => {
                                history.push('/auth/register');
                            }}>
                                <Icon type="form" />
                                Đăng kí
                            </div>
                        }
                        key="2"
                    >
                        <Route
                            path="/auth/register"
                            render={(props) => {
                                return <RegistrationForm props={props} />;
                            }}
                        />
                    </TabPane>
                </Tabs>
            </div>
        </div>
    );
}
