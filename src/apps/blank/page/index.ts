export {default as Home} from './home/home';
export {default as Login} from './auth/auth';
export {default as UserPage} from './user';
export {default as Location} from './location';