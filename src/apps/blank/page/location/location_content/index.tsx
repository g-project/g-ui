import React, { useState } from 'react';
import { Pagination } from 'antd';
import FeedbackShow from 'apps/blank/component/feedback';
import useDetailGetData from 'apps/hook/useDetailGetData';

const LocationContent = (props: any) => {
    const { dataLocation, id } = props;
    const [page, setPage] = useState(1);
    const queryFeedback = `feedback?where={"location_id":"${id}"}&max_results=5&sort=[("_created", -1)]&page=${page}`;

    const [, data, ,] = useDetailGetData<[]>(queryFeedback);

    function onChangePage(current: number) {
        setPage(current);
        console.log(current);
    }

    function onShowSizeChange(current: number) {
        console.log(current);
    }

    if (!data) return null;

    return (
        <div className='location-info-content'>
            <div className='text-4xl font-extrabold location-info-content-title'>{dataLocation.title}</div>
            <div>Địa chỉ: {dataLocation.address || 'Việt Nam'}</div>
            <div>Tổng số bình luận: {(data as any)._meta.total}</div>
            <div className="flex justify-center flex-col items-center">
                {(data as any)._items.map((item: any) => {
                    return (
                        <FeedbackShow parent={[item]} finish user />
                    );
                })}
            </div>
            {((data as any)._items.length < 1 && page === 1) ?
                <div>
                    No data
                </div>
                : <div className="pagination-page">
                    <Pagination
                        onChange={onChangePage}
                        defaultCurrent={page}
                        total={(data as any)._meta.total}
                        pageSize={5}
                        onShowSizeChange={onShowSizeChange}
                    />
                </div>
            }
        </div>
    );
};

export default LocationContent;