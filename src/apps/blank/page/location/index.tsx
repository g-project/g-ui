import React from 'react';
import './style.scss';
import { Route, useHistory } from 'react-router';
import { DataProfile } from '@foundation/access';
import { Loading } from '@foundation/component';
import { Button } from 'antd';
import { useSelector } from 'react-redux';
import useGetData from 'apps/hook/useGetData';
import LocationContent from './location_content';
import { randomStr } from 'util/function';

const ButtonAdd = (props: any) => {
    const id = props.id;
    const city_id = props.city_id;
    const history = useHistory();
    const user_id = useSelector((state: any) => state.auth.auth.userInfo.user_id);
    const user_city = useSelector((state: any) => state.auth.auth.userInfo.user_city);

    const post_query = 'feedback_saving/';
    const post_data = { location_id: id, user_id: user_id, location_city: city_id, user_city: user_city, partner_id: '', season_id: '', comment: '', score: 0 };
    const post_Location = async () => {
        try {
            await DataProfile.Post(post_query, { data: post_data });
            history.push(`/user/route?${randomStr(10,'1234qwerty')}`);
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <Button type="primary" block onClick={post_Location}>
            Thêm vào Hành trình
        </Button>
    );
};

const LoginButton = () => {
    const history = useHistory();
    return (
        <div>
            <Button type="primary" className='mb-3' block onClick={() => { history.push('/auth', history.location.pathname); }}>
                Đăng Nhập để lưu điểm đến!
            </Button>
            <Button type="primary" block onClick={() => { history.push('/auth/register', history.location.pathname); }}>
                Đăng Kí nếu bạn chưa có tài khoản!
            </Button>
        </div>
    );
};

const LocationShow = (props: any) => {
    const id = props.match.params.id;
    const queryLocation = `location/${id}`;
    const auth = useSelector((state: any) => state.auth.auth.isAuthenticated);

    const queryFeedback = `feedback?where={"location_id":"${id}"}&max_results=100`;

    const [, dataLocationFeedback, ,] = useGetData<[]>(queryFeedback);

    const [loading, dataLocation, ,] = useGetData<{}>(queryLocation);

    if (!dataLocation || !dataLocationFeedback) return null;

    if (loading) {
        return (
            <Loading />
        );
    }

    return (
        <div className="flex-1" style={{ backgroundImage: `url(/data/img/${id}.jpg)`, backgroundSize: 'cover'}}>
            <div className='location-container' >
                <div className='location-info'>
                    <div className='location-info-image'>
                        <img style={{ 'height': '200px' }} src={require(`apps/data/img/${id}.jpg`) || 'https://upload.wikimedia.org/wikipedia/commons/4/4e/Th%C3%A1p_R%C3%B9a_3.jpg'} alt='' />
                        {auth ? <ButtonAdd id={id} city_id={(dataLocation as any).city_id} /> : <LoginButton />}
                        <div className="mapouter">
                            <div className="gmap_canvas">
                                <iframe
                                    title={(dataLocation as any).title}
                                    width="300"
                                    height="300"
                                    id="gmap_canvas"
                                    src={`https://maps.google.com/maps?q=${(dataLocation as any).title}&t=&z=13&ie=UTF8&iwloc=&output=embed`}
                                    frameBorder="0"
                                    scrolling="no"
                                    marginHeight={0}
                                    marginWidth={0}>
                                </iframe>
                                <a href="https://www.couponflat.com">couponflat.com</a>
                            </div>
                        </div>
                    </div>
                    <LocationContent dataLocation={dataLocation} id={id} city_id={(dataLocation as any).city_id} />
                </div>
            </div>
        </div>
    );
};

const Location = (props: any) => {
    return (
        <Route path='/location/:id' render={(props) => <LocationShow {...props} />} />
    );
};

export default Location;
