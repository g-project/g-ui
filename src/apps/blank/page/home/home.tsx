import React from 'react';
import './style.scss';
import HomeAuth from './home_Auth';
import { useSelector } from 'react-redux';
import { Button } from 'antd';
import { useHistory } from 'react-router';
import { ShowTrending } from './home_Auth/showTrending';

const LinkToSearch = () => {
    const history = useHistory();
    return (
        <div>
            <Button type="primary" block onClick={() => { history.push('/search'); }}>
                Tìm địa điểm thích hợp với nhu cầu của bạn!
            </Button>
            <ShowTrending />
        </div>
    );
};


const Home = () => {
    const auth = useSelector((state: any) => state.auth.auth.isAuthenticated);
    return (
        <div className="p-5">
            {
                auth ? <HomeAuth /> : <LinkToSearch />
            }
        </div>
    );
};

export default Home;
