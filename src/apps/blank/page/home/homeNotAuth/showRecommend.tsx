import React from 'react';
import useGetData from 'apps/hook/useGetData';
import { Link } from 'react-router-dom';
import Card from 'apps/blank/component/card';

const ShowRecommend = (props: any) => {
    const query = `/location?where={"_id": "${props.location_id}"}`;
    const [, data, ,] = useGetData<{}>(query);
    if (!data) {
        return null;
    }
    return (
        <Link to={`/location/${props.location_id}`}>
            <Card title={(data as any)[0].title} id={props.location_id} />
        </Link>
    );
};

export default ShowRecommend;