import React from 'react';
import { Link } from 'react-router-dom';
import Card from 'apps/blank/component/card';

const ShowLocation = (props: any) => {
    return (
        <>
            {props.location.length > 0 ?
                <div className="location-list">
                    {props.location.map((item: any, i: number) => {
                        return (
                            <Link to={`/location/${item._id}`}>
                                <Card title={(item as any).title} id={item._id}/>
                            </Link>
                        );
                    })}
                </div> :
                null
            }
        </>
    );
};

export default ShowLocation;