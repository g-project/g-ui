import React, { useState } from 'react';
import Selector from 'apps/blank/component/selector';
import { DataProfile } from '@foundation/access';
import { Button, Icon } from 'antd';
import { season, user_city, partner } from 'apps/data';
import ShowLocation from './showLocation';
import ShowRecommend from './showRecommend';
import { Loading } from '@foundation/component';


const HomePublic = () => {
    const [location_city_id, setLocationCity] = useState('Bạn Muốn Đi Đâu?');
    const [partner_id, setPartnerId] = useState('Đi Cùng Ai');
    const [user_city_id, setUserCity] = useState('Bạn đang ở đâu?');
    const [season_id, setSeasonId] = useState('Đi Vào Lúc Nào');
    const [location, setLocation] = useState([]);
    const [location_recommended, setLocationRecommended] = useState([]);
    const [state, setState] = useState(false);
    const [loading, setLoading] = useState(false);

    const LocationCityChange = async (value: string) => {
        setLocationCity(value);
        getCity(value);
    };

    const UserCityChange = (value: string) => {
        setUserCity(value);
    };

    const PartnerChange = (value: string) => {
        setPartnerId(value);
    };

    const SeasonChange = (value: string) => {
        setSeasonId(value);
    };

    async function del_request() {
        try {
            await DataProfile.Delete('/request');
        } catch (e) {
            return e;
        }
    }

    async function del_fb() {
        try {
            await DataProfile.Delete('/del_fb');
        } catch (e) {
            return e;
        }
    }

    async function getCity(item: any) {
        try {
            const data = await DataProfile.Get(`/city?where={"title": "${item}"}`);
            const dataLocations = await DataProfile.Get(`/location?where={"city_id": "${data.data._items[0]._id}"}`);
            setLocation(dataLocations.data._items);
            setLocationRecommended([]);
        } catch (e) {
            return e;
        }
    }

    async function send_require_new() {
        del_fb();
        const array = location.map((item: any) => {
            return {
                location_id: item._id,
                user_id: 'New',
                location_city: item.city_id,
                user_city: user_city_id,
                partner_id: partner_id,
                season_id: season_id,
                score: 1
            };
        });
        try {
            await DataProfile.Post('/request', { data: array });
            getRecommend();
        } catch (e) {
            return [];
        }
    }

    async function getRecommend() {
        try {
            await setLoading(true);
            const data = await DataProfile.Get('/recommend');
            if (data.data.status === 'ok') {
                const sortdata = data.data.recommend.sort(function (a: any, b: any) {
                    let a1 = a.val;
                    let b1 = b.val;
                    if (a1 === b1) return 0;
                    else return a1 < b1 ? 1 : -1;
                });
                setLocationRecommended(sortdata);
            }
        } catch (e) {
            return [];
        } finally {
            await setLoading(false);
        }

    }

    return (
        <>
            <div className={state ? '' : 'middle_selector'}>
                <div className="flex items-center flex-col" style={state ? { 'paddingTop': '20px' } : { 'height': '600px' }}>
                    {state ? null : <img src={require('./logo.png')} style={{ 'width': '200px', 'padding-bottom': '10px' }} alt="logo" />}
                    <div className='italic flex items-center'>
                        <div className="seletor">
                            <Selector defaultValue={location_city_id} onChange={(value: any) => { setState(true); LocationCityChange(value); }} data={user_city} />
                        </div>
                        <Icon type="caret-right" />
                        <div className="seletor">
                            <Selector defaultValue={user_city_id} onChange={UserCityChange} data={user_city} />
                        </div>
                        <Icon type="caret-right" />
                        <div className="seletor">
                            <Selector defaultValue={partner_id} onChange={PartnerChange} data={partner} />
                        </div>
                        <Icon type="caret-right" />
                        <div className="seletor">
                            <Selector defaultValue={season_id} onChange={SeasonChange} data={season} />
                        </div>
                        <Icon type="caret-right" />
                        <Button type="primary" onClick={() => { send_require_new(); del_request(); }}>Tìm kiếm</Button>
                    </div>
                </div>
            </div>
            {
                loading
                    ?
                    <Loading />
                    :
                    (
                        location_recommended.length > 0
                            ?
                            <div className="location-list">
                                {location_recommended.map((item: any) => {
                                    return (
                                        <ShowRecommend location_id={item.location_id} />
                                    );
                                })}
                            </div>
                            :
                            <ShowLocation location={location} />
                    )
            }
        </>
    );
};

export default HomePublic;

