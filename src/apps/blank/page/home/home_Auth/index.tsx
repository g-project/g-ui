import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'antd';
import { useHistory } from 'react-router';
import { DataProfile } from 'apps/base/access';
import { UPDATE_USER_INFO } from 'apps/auth/state/auth/const';
import { RecommendArea } from './recommendRoute';
import { ShowTrending } from './showTrending';
import { NotificationArea } from './notificationInvitation';

const HomeAuth = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const auth = useSelector((state: any) => state.auth.auth.isAuthenticated);
    const id = useSelector((state: any) => state.auth.auth.userInfo.user_id);
    const email = useSelector((state: any) => state.auth.auth.userInfo.email);
    const username = useSelector((state: any) => state.auth.auth.userInfo.full_name);
    const user_city = useSelector((state: any) => state.auth.auth.userInfo.user_city);

    async function update_recommend() {
        try {
            const data = await DataProfile.Post('recommend', { data: { 'id': id } });
            console.log('recommend', data);
            await dispatch({ type: UPDATE_USER_INFO, payload: { email: email, full_name: username, user_id: id, user_city: user_city, recommend: data.data.recommend } });
        } catch (e) {
            return e;
        }
    }

    return (
        <div className="flex container-home flex-col">
            <NotificationArea />
            <div>
                <Button type="primary" block onClick={() => { history.push('/search'); }}>
                    Tìm địa điểm thích hợp với nhu cầu của bạn!
                </Button>
            </div>
            <ShowTrending />
            {auth
                ?
                <div>
                    <div className="lb-recommend" onClick={update_recommend}>
                        Gợi ý của bạn
                    </div>
                    <RecommendArea />
                </div>
                :
                null
            }
        </div>
    );
};

export default HomeAuth;
