import React, { ReactChild } from 'react';
import { useSelector } from 'react-redux';
import { Button } from 'antd';
import { DataProfile } from 'apps/base/access';
import useGetData from 'apps/hook/useGetData';
import { Loading } from '@foundation/component';
import { useHistory } from 'react-router';

const Notification = (props: any) => {
    const history = useHistory();
    const queryUser = `/user/${props.item.invitor}`;
    const queryLocation = `/location/${props.item.location_id}`;
    const [, dataUser, ,] = useGetData<[]>(queryUser);
    const [, dataLocation, ,] = useGetData<[]>(queryLocation);
    if (!dataUser || !dataLocation) {
        return <Loading />;
    }

    function reloadData() {
        return props.reloadData();
    }

    async function defNotif() {
        const headers = {
            'Content-Type': 'application/json',
            'If-Match': props.item._etag
        };
        await DataProfile.Delete(`invitation/${props.item._id}`, {headers: headers});
        reloadData();
    }

    async function acceptNotif() {
        const headers = {
            'Content-Type': 'application/json',
            'If-Match': props.item._etag
        };
        try {
            await DataProfile.Update(`invitation/${props.item._id}`, {headers: headers, data: {'status': 'accepted'}});
        } catch (e) {
            console.log('err', e);
        }
        history.push('/user/route');
    }

    return (
        <div className="flex-center">
            <h1>
                Bạn có một lời mời từ {(dataUser as any).username} Tham gia đi {(dataLocation as any).title}
            </h1>
            <Button type="primary" className="notif-btn" onClick={acceptNotif}>Đồng Ý</Button>
            <Button type="danger" className="notif-btn" onClick={defNotif}>Từ Chối</Button>
        </div>
    );
};

export const NotificationArea = () => {
    const user = useSelector((state: any) => state.auth.auth.userInfo.user_id);
    const query = `/invitation?where={"invitee":"${user}","status": "pending"}`;
    const [loading, data, ,reloadData] = useGetData<[]>(query);
    if (loading || !data) {
        return <Loading />;
    }
    const notifs: ReactChild[] = [];
    if (data.length > 0) {
        data.forEach((item: any) => {
            console.log('data invitation', item);
            notifs.push(<Notification item={item} reloadData={reloadData}/>);
        });
        return <>{notifs}</>;
    } else {
        return <div>No Invitation For You</div>;
    }
};