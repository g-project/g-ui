import React from 'react';
import { Loading } from '@foundation/component';
import useGetData from 'apps/hook/useGetData';
import { RecommendRoute } from './recommendRoute';

export const ShowTrending = () => {
    const query = '/get_trend';
    const [loading, data, ,] = useGetData<{}>(query);
    if (loading) {
        return <Loading />;
    }
    if (!data) {
        return null;
    }
    
    return (
        <>
            <div className="lb-recommend">
                Thịnh Hành
            </div>
            <div className="location-list">
                {(data as any).recommend.map((item: any, index: number) => {
                    return <RecommendRoute key={index} item={item} />;
                })}
            </div>
        </>
    );
};