import React from 'react';
import { Link } from 'react-router-dom';
import useGetData from 'apps/hook/useGetData';
import { useSelector } from 'react-redux';
import Card from 'apps/blank/component/card';

export const RecommendRoute = (props: any) => {
    const query = `location/${props.item.location_id}`;
    const [, data, ,] = useGetData(query);
    if (!data) return null;
    return (
        <Link to={`/location/${props.item.location_id}`}>
            <Card title={(data as any).title} id={props.item.location_id} />
        </Link>
    );
};

export const RecommendArea = () => {
    const recommend = useSelector((state: any) => state.auth.auth.userInfo.recommend);
    if (!recommend) return null;
    return (
        <div className="location-list">
            {recommend.map((item: any, index: number) => { return <RecommendRoute key={index} item={item} />; })}
        </div>
    );
};