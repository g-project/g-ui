import React from 'react';
import useGetData from 'apps/hook/useGetData';
import { schema, uiSchema } from './schema.json';
import Form from 'react-jsonschema-form';
import { JSONSchema6 } from 'json-schema';
import './styles.scss';
import { DataProfile } from 'apps/base/access';
import { useHistory } from 'react-router';
import { randomStr } from 'util/function';

const UserInfo = () => {
    const history = useHistory();
    const query = '/user/info';
    const [, data, ,] = useGetData<{}>(query);
    if (!data) return null;
    const queryUser = `user/${(data as any)._id}`;

    const headers = {
        'Content-Type': 'application/json',
        'If-Match': (data as any)._etag
    };

    async function onSubmit(e: any) {
        delete e.formData._created;
        delete e.formData._etag;
        delete e.formData._links;
        delete e.formData._updated;
        
        try {
            const res = await DataProfile.Update(queryUser, { data: e.formData, headers: headers });
            console.log(res);
            if(res.status === 200){
                history.push(`/user/user_info?new${randomStr(8, '123asdfg')}`);
            }
        } catch (e) {
            console.log('Lo', e);
        } finally {
            
        }

    };
    
    return (
        <div className="flex ">
            <div className="user-avatar w-64">
                <img style={{ 'boxShadow': '-9px 7px 50px -4px rgba(0,0,0,0.75)' }} src="https://divui.com/blog/wp-content/uploads/2018/04/trickeye-museum-seoul-cover.jpg" alt="avatar" />
            </div>
            <div className="user-infor bootstrap">
                <Form
                    schema={schema as JSONSchema6}
                    uiSchema={uiSchema as JSONSchema6}
                    formData={data}
                    onSubmit={onSubmit}
                />
            </div>
        </div>
    );
};

export default UserInfo;