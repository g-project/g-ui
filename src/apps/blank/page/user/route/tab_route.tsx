import React, { useState } from 'react';
import { DataProfile } from 'apps/base/access';
import FeedbackShow from '../../../component/feedback';
import { Pagination } from 'antd';
import { Loading } from '@foundation/component';
import useDetailGetData from 'apps/hook/useDetailGetData';
import './styles.scss';

type TabRouteProps = {
    id: string
    finish?: boolean
}

const TabRoute = (props: TabRouteProps) => {
    const [page, setPage] = useState(1);
    const query = `${props.finish ? 'feedback' : 'feedback_saving'}?where={"user_id":"${props.id}"}&max_results=5&sort=[("_created", -1)]&page=${page}`;
    const [loading, data, , reloadData] = useDetailGetData<[]>(query);

    const deleteItem = async (url: string, headers: any) => {
        const post_saving = url;
        try {
            await DataProfile.Delete(post_saving, { headers: headers });
            reloadData();
        } catch (e) {
            console.log(e);
        }
    };

    const updateItem = async (url: string, headers: any, data: any) => {
        const post_saving = url;
        try {
            await DataProfile.Update(post_saving, { data: data, headers: headers });
            reloadData();
        } catch (e) {
            console.log(e);
        }
    };

    if (!data) return null;
    if (loading) {
        return (
            <Loading />
        );
    }

    function onChange(current: number) {
        setPage(current);
        console.log(current);
    }
    console.log(data);

    return (
        <>
            <div className="flex justify-center flex-col items-center">
                {(data as any)._items.map((item: any) => {
                    return <FeedbackShow
                        parent={[item]}
                        finish={props.finish}
                        deleteItem={(url: string, headers: any) => deleteItem(url, headers)}
                        updateItem={(url: string, headers: any, data: any) => updateItem(url, headers, data)}
                        postItem={reloadData}
                    />;
                })}
            </div>
            {((data as any)._items.length < 1) ?
                <div className='text-nodata'>
                    No data
                </div>
                : <div className="pagination-page">
                    <Pagination
                        onChange={onChange}
                        defaultCurrent={page}
                        total={(data as any)._meta.total}
                        pageSize={5}
                    />
                </div>
            }
        </>
    );
};

export default TabRoute;