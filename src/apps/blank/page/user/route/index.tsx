import React from 'react';
import { Tabs } from 'antd';
import TabRoute from './tab_route';
import { useSelector } from 'react-redux';
import { Route, useHistory} from 'react-router';
import './styles.scss';
import { randomStr } from 'util/function';
const { TabPane } = Tabs;

const RoutePath = () => {
    const history = useHistory();
    const id = useSelector((state: any) => state.auth.auth.userInfo.user_id);
    console.log(history);

    function changeTabs(key: string){
        key==='1'?history.push(`/user/route?${randomStr(10, '1234gflkkn')}`):history.push(`/user/route/finish?${randomStr(10, '1234gflkkn')}`);
    }
    
    return (
        <div className="flex justify-center">
            <Tabs type="card" onChange={changeTabs} activeKey={history.location.pathname === '/user/route/finish'?'2':'1'} className='review-are' >
                <TabPane tab="Chưa Hoàn Thành" key="1" className='flex flex-col' >
                    <Route path='/user/route' render={() => <TabRoute id={id} />} />
                </TabPane>
                <TabPane tab="Đã Hoàn Thành" key="2" className='flex flex-col'>
                    <Route path='/user/route/finish' render={() => <TabRoute id={id} finish />} />
                </TabPane >
            </Tabs>

        </div>
    );
};
export default RoutePath;