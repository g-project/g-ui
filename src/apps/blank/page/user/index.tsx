import React from 'react';
import './style.scss';
import { Route } from 'react-router-dom';
import UserInfo from './info/info';
import RoutePath from './route';
import { Button, Icon } from 'antd';
import { useHistory } from 'react-router';
import { randomStr } from 'util/function';

const menu_tabs = [
    {
        key: 1,
        title: 'User Information',
        path: 'user_info'
    },
    {
        key: 2,
        title: 'Route',
        path: 'route'
    }
];

const UserPage = () => {
    const history = useHistory();
    return (
        <div className="user_container flex-1">
            <div className="dashboard flex-1">
                <div className="menu-left">
                    {menu_tabs.map((item: any) => {
                        return (
                            <Button key={item.key} type="primary" className="menu-tab" onClick={() => history.push(`/user/${item.path}?${randomStr(10, '1234gflkkn')}`)}>
                                {item.title}
                                <Icon type="right" />
                            </Button>
                        );
                    })}
                </div>
                <div className="content-right">
                    <Route
                        path="/user/user_info"
                        render={() => {
                            return <UserInfo />;
                        }}
                    />
                    <Route
                        path="/user/route"
                        render={() => {
                            return <RoutePath />;
                        }}
                    />
                </div>
            </div>
        </div>

    );
};

export default UserPage;
