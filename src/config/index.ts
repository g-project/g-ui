import { ENV } from '@foundation/utils';

export const SITE_NAME = ENV('SITE_NAME', 'ABX');
export const APP_NAME = ENV('APP_NAME', 'React Base');

export const LOGO_URL = ENV('LOGO_URL', '');

export const API_URL = ENV('API_URL');
export const AUTH_URL = ENV('AUTH_URL');
export const REPORT_URL = ENV('REPORT_URL');

export const PROFILE_URL = ENV('PROFILE_URL');
export const LOGIN_URL = ENV('LOGIN_URL');
export const LOGOUT_URL = ENV('LOGOUT_URL');

// Todos: Find another way !!!
export const NAV = [
    {
        title: 'Home',
        key: 'home',
        link: '/',
    }
];