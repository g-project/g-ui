import React from 'react';
import AppWrapper from 'apps/base/component/app_wrapper';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { NotFoundPage } from '@foundation/page';
import ErrorBoundary from '@foundation/component/error-wrapper';
import BaseApplication from '@foundation/app';
import { RouteRegistry } from '@foundation/registry';
import PrivateRoute from 'apps/base/component/private_route';

import 'styles/index.less';
import 'styles/index.scss';
import 'styles/index.css';

const InstalledModule = {
    auth: require('apps/auth'),
    blank: require('apps/blank')
};

export default class RootApplication extends BaseApplication {
    getInstalledModule() {
        return InstalledModule;
    }
    renderRoute() {
        const allRoute = RouteRegistry.ALL;
        return Object.keys(allRoute).map(path => {
            const route = allRoute[path];
            const { secure = true, ...props } = route;
            const Comp = secure ? PrivateRoute : Route;
            return (
                <Comp key={path} path={path} {...props} />
            );
        });
    }
    renderApp() {
        return (
            <ErrorBoundary>
                <Provider store={this.store}>
                    <ConnectedRouter history={this.history}>
                        <AppWrapper>
                            <Switch>
                                {this.renderRoute()}
                                <Route component={NotFoundPage} />
                            </Switch>
                        </AppWrapper>
                    </ConnectedRouter>
                </Provider>
            </ErrorBoundary>
        );
    }
    render() {
        const { loading } = this.state;
        if (loading) {
            return (
                <div>Loading, please wait</div>
            );
        }
        return this.renderApp();
    }
}
