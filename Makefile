AWS:=$(shell which aws)
PRODUCTION_BUCKET:=teaco-platform.self-service-center
BUILD_DIR=build
# develop | production | certify
DEPLOY_TARGET=develop

run:
	@REACT_APP_ENV=./.env.${DEPLOY_TARGET} npm start

test:
	@npm run test

lint:
	@npm run lint

build:
	@REACT_APP_ENV=.env.${DEPLOY_TARGET} npm run build

change-log:
	@git-chglog -o CHANGELOG.md
	@git describe --tags $$(git rev-list --tags --max-count=1) > VERSION

doc:
	docsify serve docs

deploy: build s3-sync-quick

deploy-force: build s3-sync-force

s3-sync-quick:
	${AWS} --profile ssc_deploy s3 sync $(BUILD_DIR)/ s3://${PRODUCTION_BUCKET}/${DEPLOY_TARGET}/

s3-sync-force:
	${AWS} --profile ssc_deploy s3 cp --recursive $(BUILD_DIR)/ s3://${PRODUCTION_BUCKET}/${DEPLOY_TARGET}/

commit:
	git add .
	git commit && git push

.PHONY: build commit
