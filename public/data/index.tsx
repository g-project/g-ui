const season = [
    { id: '5db949ac6b968a053f7c6490', title: 'Tháng 1 - Tháng 3' },
    { id: '5db949df6b968a053f7c6491', title: 'Tháng 4 - Tháng 6' },
    { id: '5db949fc74f75144c9bc4507', title: 'Tháng 7 - Tháng 9' },
    { id: '5db94a0474f75144c9bc4508', title: 'Tháng 10 - Tháng 12' },
];

const partner = [
    { id: '5db94aa400c50c0b9316cc35', title: 'Một Mình' },
    { id: '5db94aae00c50c0b9316cc36', title: 'Bạn Bè' },
    { id: '5db94abf00c50c0b9316cc37', title: 'Gia Đình' },
    { id: '5db94ad600c50c0b9316cc38', title: 'Người Yêu' },
];

const user_city = [
    { id: 'Hà Nội', title: 'Hà Nội' },
    { id: 'Đà Nẵng', title: 'Đà Nẵng' },
    { id: 'Hồ Chí Minh', title: 'Hồ Chí Minh' },
    { id: 'Thừa Thiên Huế', title: 'Thừa Thiên Huế' },
    { id: 'Hải Phòng', title: 'Hải Phòng' },
    { id: 'Quảng Bình', title: 'Quảng Bình' },
    { id: 'An Giang', title: 'An Giang' },
    { id: 'Bà Rịa - Vũng Tàu', title: 'Bà Rịa - Vũng Tàu' },
    { id: 'Bắc Giang', title: 'Bắc Giang' },
    { id: 'Bến Tre', title: 'Bến Tre' },
    { id: 'Bình Định', title: 'Bình Định' },
    { id: 'Bình Dương', title: 'Bình Dương' },
    { id: 'Cà Mau', title: 'Cà Mau' },
    { id: 'Đắk Lắk', title: 'Đắk Lắk' },
    { id: 'Đắk Nông', title: 'Đắk Nông' },
    { id: 'Điện Biên', title: 'Điện Biên' },
    { id: 'Gia Lai', title: 'Gia Lai' },
    { id: 'Hải Dương', title: 'Hải Dương' },
    { id: 'Hoà Bình', title: 'Hoà Bình' },
    { id: 'Kiên Giang', title: 'Kiên Giang' },
    { id: 'Lai Châu', title: 'Lai Châu' },
    { id: 'Lạng Sơn', title: 'Lạng Sơn' },
    { id: 'Nam Định', title: 'Nam Định' },
    { id: 'Nghệ An', title: 'Nghệ An' },
    { id: 'Phú Thọ', title: 'Phú Thọ' },
    { id: 'Quảng Nam', title: 'Quảng Nam' },
    { id: 'Sơn La', title: 'Sơn La' },
    { id: 'Thái Bình', title: 'Thái Bình' },
    { id: 'Vĩnh Long', title: 'Vĩnh Long' },
    { id: 'Yên Bái', title: 'Yên Bái' }
];

export {season, partner, user_city};