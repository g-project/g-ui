# React boilerplate

## Initial

```bash
git clone --recurse-submodules git@gitlab.com:abx-fe/react-boilerplate.git
```

## Document

- To read the document you have to run the command below:

```bash
make doc
```

- If you have got any problem, take a look at <https://docsify.js.org/#/quickstart>
